"""
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
"""
"""
Simulation constants.
Imported from RTSimulator.py, KiteViewer3.py, KPS4P.py.
"""

AZIMUTH = 0
ELEVATION = 10
SEGMENTS = 6
L_0 = 25






	#----------------------#
	#----STANDARD CASES----#
	#----------------------#

	#-----ELEVATION 60-----#

	#AZIMUTH 0
	#pod_pos = np.array(( 75.000000000000028, 9.9999999999999995e-07, 129.9038105676658))

	#AZIMUTH 15
        #pod_pos = np.array(( 72.444436971680133, 19.411429382689061, 129.9038105676658))

	#AZIMUTH 30
	#pod_pos = np.array(( 64.951905283832929, 37.500000999999997, 129.9038105676658))

	#----ELEVATION 35-----#

	#AZIMUTH 0
	#pod_pos = np.array(( 122.87280664334877, 9.9999999999999995e-07, 86.036465452656913))

	#AZIMUTH 15
	#pod_pos = np.array(( 122.87280664334877, 9.9999999999999995e-07, 86.036465452656913))
