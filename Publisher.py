"""
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
"""
""" This module provides the class Publisher for publishing ParticleSystem messages on port 5575. """
import zmq
import time as ti
import asset.system_pb2 as system

# pylint: disable=E1101
class Publisher(object):
    """Publisher for ZeroMQ messages. Is publishing on port ParticleSystem messages
       and EstimatedSystemState messages on port 5575. """
    def __init__(self):
        self.context = zmq.Context()
        self.sock = self.context.socket(zmq.PUB)
        self.sock.bind('tcp://*:5575')
        ti.sleep(0.5) # wait until the connection is established before sending any messages
        self.sys = system.ParticleSystem()
        self.time = 0.0
        self.counter = 0
        self.event_counter = 0
        self.pilot_counter = 0
        self.pod_state_counter = 0

    def publishParticleSystem(self, pos, v_reel_out, v_app, l_tether, force, azimuth, elevation, \
                                    vel_kite, cpu_time, rel_depower, prediction=[]):
        """
        Publish the ParticleSystem message (for the KiteViewer)
        Parameters:
        pos: vector of the positions of the particles """
        self.sys.Clear()
        self.sys.time = self.time
        self.sys.cpu_time = cpu_time
        self.sys.rel_depower = rel_depower
        self.time += 0.05 # one new message every 50 ms
        self.sys.counter = self.counter
        self.counter += 1
        #for particle in range(segments+1):
        for particle in range(pos.shape[0]):
            position = self.sys.positions.add()
            position.pos_east  = pos[particle][0]
            position.pos_north = pos[particle][1]
            position.height    = pos[particle][2]
        for particle in range(len(prediction)):
            position = self.sys.pos_predicted.add()
            position.pos_east  = prediction[particle][0]
            position.pos_north = prediction[particle][1]
            position.height    = prediction[particle][2]
        velocity = self.sys.velocities.add()
        velocity.v_east  = vel_kite[0]
        velocity.v_north = vel_kite[1]
        velocity.v_up    = vel_kite[2]
        self.sys.v_reel_out = v_reel_out
        self.sys.l_tether   = l_tether
        self.sys.force      = force
        self.sys.azimuth, self.sys.elevation = azimuth, elevation
        self.sys.v_app_x, self.sys.v_app_y, self.sys.v_app_z = v_app[0], v_app[1], v_app[2]
        binstr = chr(21) + self.sys.SerializeToString()
        self.sock.send(binstr)
        # print "vel_kite: ", vel_kite

    def sendMessage(self, message, msg_type):
        """ Send protocol buffer encoded message over the zeromq socket, that was assigned in the
        method __init__. """
        binstr = chr(msg_type) + message.SerializeToString()
        #print binstr.encode('hex_codec')
        self.sock.send(binstr)

    def close(self):
        """ Close zeromq socket. This method should must always be called before the program
        finishes, otherwise the socket stays blocked. """
        self.sock.close()
