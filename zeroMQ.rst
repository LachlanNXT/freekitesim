Introduction to ZeroMQ
======================

ZeroMQ is a socket network library written in C++ that adds a layer of
abstraction on top of the BSD socket API. This makes it easy to use and
releases the user from most lower-level implementation work. 
In contrast to the commonly known transport protocols TCP and UDP, zeroMQ
utilizes a "Message" transport protocol. Furthermore ZeroMQ has asynchronous
timing, that is it can queue upcoming messages if a peer is unavailable but also
all events such as connection teardown, reconnect and delivery are handles by
the library itself. 
Finally, ZermMQ adds support for many-to-many connections, meaning that a socket
may send data to multiple peers(connect()) as well as recieve(bind()) from multiple peers. 

As such ZeroMQ can be used to handle internet connections as well as
inter-process communication. This e.g. allows for a modular software consisting
of multiple parts which exchange data via ZeroMQ sockets. These can be written
in different programming languages and even run on different machines. 

Example
----------------------

Usually it is easiest to understand by providing an example. The most basic
application of ZeroMQ one could imagine is for two programs to exchange a "Hello
World!" message. 
The two programs have to do different things. One is the publisher which sends
data to the other process, the other acts as a receiver to listen to the message
and process it.

::

    import time
    import zmq

    context = zmq.Context()        
    sock = context.socket(zmq.PUB)
    sock.bind('tcp://*:5575')
    time.sleep(0.1)

    for i in range(10):
        sock.send("Hello World!")
        time.sleep(0.5)
    sock.close()

Let's go through this line by line. The imports should be clear, time(only to
introduce a delay) and zmq(ZeroMQ).
The first step is to create a context with ``zmq.Context()`` which is a
container for all sockets and can be considered an instance of ZeroMQ. There is
usually only one context for the whole process needed. 
Next a socket is created, in this case a publishing socket(``zmq.PUB``). There
are various different types of sockets. They usually work in pairs, that is the
receiver sockets needs to have a complement type to the publisher it tries to
listen to.
The socket now needs to be given a port number, which can be any port in the
range of 1 to 65535, although no ports in the range 0-1024 should be used(only
possible as root anyways) as they are reserved for commonly used applications. 
Also ports in the high ranges can be used by applications so before using a port 
you should check if the ports a free on your machine (for example using a program 
like nmap). In this case we went for the TCP port 5575.
In order to be sure that the socket is correctly bound to the port before
continuing, a short dealy is introduced with ``time.sleep(0.1)``.
And that is all for the setup of the publisher, very easy. 

Now we can publish messages on the chosen port. In this case we just send "Hello
World!" ten times and then close the socket again.

Now for the receiver which is just as simple.

::

    import time
    import zmq
    
    start_time = time.time()
    context = zmq.Context()
    sock = context.socket(zmq.SUB)
    sock.connect('tcp://localhost:5575')
    sock.setsockopt(zmq.SUBSCRIBE, "")
    time.sleep(0.1)
    
    msgs_received = 0
    d_time = time.time()-start_time
    while msgs_received<10 and d_time<20:
        string = ""
        events = sock.poll(100)
        if events > 0:
            string = sock.recv()
            msgs_received += 1
        d_time = time.time()-start_time
        print string
        print d_time
    sock.close()

First the start time is determined in order to later create a timeout event for
the receiver and the context created as before. Secondly, a socket is
initialized, this time as a subscriber(``zmq.SUB``). Subscribers are the
complement for publishers, which was used for the previously created publisher
script.
Again the socket connects to the tcp port of the localhost. This time also a
filter for the messages has to be set, which determines to what kind of messages
we listen to. The function ``setsockopt`` determines what messages shall be
listened to. By specifying an empty string, all messages are received. 
Finally a short delay will make sure the socket is setup successfully before
continuing. 

At this point the receiver is setup and running and the messages can be
received. For that a continuous loop is entered with two conditions, that less
than 10 messages have been received and that the receiver is running for less
than 20 seconds. 
In the loop, the socket is first polled to check whether new messages are
available. The number specifies the timeout for this polling in milliseconds. If
no events happened, only the time is calculated and the loop continues. In case
a message is available, the message is received with ``sock.recv()``. The
receiving is a **blocking** function, i.e. it continues until a message is received,
therefore polling is done which has a timeout if no messages are happening.

If everything works as it should you can run both programs and should see the
"Hello World!" messages in the receiver. 
