/*
RE_FV_01   Data View, needed for manual steering.
RE_FV_01.1 The FrontView program must display the elevation and azimuth angle of the kite in a
           quarter sphere. An orthographic projection shall be used.
RE_FV_01.2 The position must be displayed within an upper half circle with projected spherical
           grid lines. There shall be grid lines for -90 degree to +90 degree within steps of
           10 degree. The result will look as shown in figure 1. (The figure 8 input fields
           shall not be included)
RE_FV_01.3 Heading and course must be displayed. A blue arrow shows the heading a green arrow
           shows the course.
RE_FV_01.4 A legend for the used heading and course colours must be shown in the top left corner
           including numerical values in degrees.
RE_FV_01.5 Red rooftop. A red rooftop is displayed at an altitude of 300m (altitude variable)
RE_FV_01.6 A blue tail must be displayed. The tail length must be variable up to 10000 points.

RE_FV_02   Display the data needed for autopilot testing.
RE_FV_02.1 Waypoints must be displayed in color magenta. Waypoints are received from the
           flight path planner as Azimuth and Elevation angles.
RE_FV_02.2 The frontview display must display the desired flight track, which is received as a
           vector of azimuth/elevation pairs from the flight path planner. The color must be
           magenta.
*/

/*
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/
#ifndef FRONTVIEW_H
#define FRONTVIEW_H

#include <QMainWindow>
#include <QtCore>
#include <QtGui>
#include <qmath.h>
#include <frontviewsettings.h>
#include "asset.system.pb.h"
#include "asset.pod.pb.h"
#include "asset.winch.pb.h"
#include "../Utils/utils.h"
#include "../ZeroMQLink/zeromqlink.h"
#include "displaybackground.h"
#include "displaytrajectory.h"
#include "displaybearing.h"
#include "displaykite.h"
#include "displayredceiling.h"
#include <QTimer>

namespace Ui {
    class FrontView;
}

// Responsibility: Reciving variables from zeroMQLink and adjusting them for the painters reference frame
class FrontView : public QMainWindow
{
    Q_OBJECT

public:
    // constructor and destructor
    explicit FrontView(QWidget *parent = 0);
    ~FrontView();

public slots:
    void handleMessageReceived(int msg_id); // Handles messages recived from asset::system::EstimatedSystemState
                                            // && asset::system::DesiredTrajectory && asset::system::Bearing
    void afterStartUp();

private:
    // methods
    void setupSettings();
    void setupGui();
    void setupCommunication();

    QPointF xyCoordinates(double elevation, double azimuth);
    QPointF headingCoordinates(double elevation, double azimuth, double hx, double hy);
    QPointF courseCoordinates(double elevation, double azimuth, double cx, double cy);

    double setCeilingParameters(double cableLength, double m_ceilingHeight);

    void closeEvent(QCloseEvent *m_event);     // Slot to save window geometry in settings upon close event
    void onEstimatedsystemStateMessage();     // asset::system::EstimatedSystemState
    void onDesiredTrajectoryMessage();        // asset::system::DesiredTrajectory
    void onBearingMessage();                  // asset::system::Bearing
    void onEventMessage();

    // attributes
    Ui::FrontView       *ui;                  // Programs user interface
    Utils                m_utils;
    QString              m_cfilename;         // name of the configuration file, passed as command line parameter
    FrontViewSettings   *m_settings;          // stores the configuration data

    QMenu               *m_viewOptions;       // menu for changing view options
    QAction *m_setTrajectoryPointSize;        // menu action for changing the trajectory point size
    QAction             *m_setTrailLength;    // menu action for changing the trail length

    QGraphicsScene      *m_scene;             // Object where items are displayed

    DisplayRedCeiling   *m_redCeilingDisplay; // Container item for the redCeiling.

    DisplayBackground   *m_backgroundDisplay; // Container item for the background items that
                                              // display instrument background (elevation and azimuth lines).
    DisplayTrajectory   *m_trajectoryDisplay; // Container item that displays the desiered trajectory.
    DisplayBearing      *m_bearingDisplay;    // Container item that displays the kite bearing.
    DisplayKite         *m_kiteStateDisplay;  // Container item for the foreground items that display
                                              // instrument foreground (kite position, trail, heading and course).

    QTimer              *m_linkTimer;         // Timer for reading messages.

    ZeroMQLink          *m_zeroMQLink;        // Link for reading estimated system state
                                              //     and planned flight path parameters.
    QByteArray          m_message;            // Container for messages.

    asset::system::EstimatedSystemState m_systemStateMsg;       // Provides Kite position, heading, and course.
    asset::system::DesiredTrajectory    m_desiredTrajectoryMsg; // Provides desired flight trajectory points.
    asset::system::Bearing              m_bearingMsg;           // Provides kite bearing in the form of points.
    asset::system::EventMsg             m_eventMsg;             // close events


    double  m_headingScale;           //
    double  m_courseScale;            //
    double  m_trailLength;
    int     m_trajectoryPointSize;
    double  m_ceilingHeight;
    int     m_period_ms;

private slots:

    void changeTrajectoryPointSize();
    void changeTrailLength();

protected slots:
    void resizeEvent(QResizeEvent *m_event);   // Virtual

protected:
    int  heightForWidth (int w);             // Virtual

};

#endif // FRONTVIEW_H
