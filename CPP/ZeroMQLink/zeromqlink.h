/*
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/
#ifndef ZEROMQLINK_H
#define ZEROMQLINK_H

#include <QObject>
#include <QTimer>
#include <QByteArray>
#include "/usr/include/zmq.hpp"
#include <QThread>
#include "abstractlink.h"

#define MAX_MSG_SIZE (80000)  // should be 500

// helper class for receiving messages without using a timer
// used by the class ZeroMQLink
class ReceiveThread : public QThread
{
    Q_OBJECT
public:
    explicit ReceiveThread(zmq::context_t* context);
    ~ReceiveThread();
    void run();
    // connect the input to the given server and port
    // can be called multiple times, to subscribe to different servers and/or ports
    // returns zero on success
    int connect_link(QString connection);
    // subscribe to messages with the given message type
    // can be called multiple times, to subscribe to different messages
    void subscribe(int msg_type);
signals:
    void messageReceived(zmq::message_t *message);

public slots:
    void finish();

protected:
    int exec();

private:
    // ZeroMQ context
    zmq::context_t* m_context;
    // ZeroMQ socket for incoming messages
    zmq::socket_t* m_subscriber;
    // pointer to a buffer for incoming messages
    zmq::message_t* m_message;
    // flag
    bool m_finish;
};

// Responsibilities:
// - provide a n:1 input  socket (subscriber) for receiving binary data messages
// - provide a 1:n output socket (publisher)  for sending binary messages
// ToDo:
// - add a QThread to avoid the dependency on a timer event for receiving messages
class ZeroMQLink : public AbstractLink
{
    Q_OBJECT
public:
    explicit ZeroMQLink(QObject *parent = 0);
    ~ZeroMQLink();
    // send the given, binary encoded message
    void sendMessage(QByteArray m_message, int msg_type = 0);
    // bind the output to the given port
    // returns zero on success
    int bind(QString connection = "tcp://*:5561");
    // connect the input to the given server and port
    // can be called multiple times, to subscribe to different servers and/or ports
    // returns zero on success
    int connect_link(QString connection = "tcp://localhost:5560");
    // subscribe to messages with the given message type
    // can be called multiple times, to subscribe to different messages
    void subscribe(int msg_type);
    // should be called after all subscriptions were done
    void start();
    // fetch the last message, that was received; should be called, after the event messageReceived was received
    QByteArray getLastMessage(int msg_type);

public slots:
    // should be called at least every 10 ms
    void onTimer(void);
    // called by the receive thread
    void onMessageReceived(zmq::message_t *message);

signals:
    // this signal is emitted, when a new input message has been detected in the onTimer slot
    void messageReceived(int msg_type);
    
private:
    // ZeroMQ context
    zmq::context_t* m_context;
    // ZeroMQ socket for outgoing messages
    zmq::socket_t* m_publisher;
    // message without leading message type
    QByteArray m_qmessage;
    // message type of the last received message
    int m_lastMsgType;
    // thread for receiving messages
    ReceiveThread* m_receiveThread;
};




#endif // ZEROMQLINK_H
