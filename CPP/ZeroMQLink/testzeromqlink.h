/*
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/
#ifndef TESTZEROMQLINK_H
#define TESTZEROMQLINK_H

#include <QObject>
#include <QString>
#include <QStringList>
#include "asset.pod.pb.h"
#include "asset.winch.pb.h"
#include "asset.system.pb.h"
#include "zeromqlink.h"

class TestZeroMQLink : public QObject
{
    Q_OBJECT
public:
    explicit TestZeroMQLink(QObject *parent = 0);
    void test();

signals:

public slots:
    void processPendingData(int msg_type);

private:
    QObject                 *m_myParent;        // needed to close the main application
    ZeroMQLink              *m_zeroMQLink;    // link for incoming messages
    asset::system::ProtoMsg m_msg;            // temp storage for incoming messages, used from processPendingData()
};


#endif // TESTZEROMQLINK_H
