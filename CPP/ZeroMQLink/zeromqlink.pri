INCLUDEPATH *= $$PWD

SOURCES += \
    $$PWD/zeromqlink.cpp \
    $$PWD/abstractlink.cpp

HEADERS += \
    $$PWD/zeromqlink.h \
    $$PWD/abstractlink.h

include(../../00AssetLib/Utils/utils.pri)
