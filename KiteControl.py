import numpy as np
import math
import matplotlib.pyplot as plt

Azimuth = 0
Elevation = 1
Heading = 2
DesiredHeading = 3
TrajectoryHeading = 4

class HelperFunctions(object):
	def __init__(self):
		pass;
	
	def dRange(self, start, stop, step):
		r = start
		while r <= stop:	
			yield r
			r += step

	def plot(self, name, x, y, axis):
		plt.figure(name)
		plt.plot(x, y)
		plt.axis(axis)
		plt.show()

	def plot1(self, name, array):
		plt.figure(name)
		plt.plot(array)
		plt.show()

	def getDistance(self, x1, y1, x2, y2):
		return math.sqrt((x2 - x1)**2 + (y2 - y1)**2)
	
	def getDistanceArray(self, x1, y1, azimuth, elevation):
		distance = []
		for x in xrange(0, len(azimuth)):
			distance.append(self.getDistance(x1, y1, azimuth[x], elevation[x]))
		return distance

	def getClosestDistance(self, kite, trajectory, headingError):
		distance = self.getDistanceArray(kite[Azimuth], kite[Elevation], trajectory[Azimuth], trajectory[Elevation])
		indexDistance = 0
		minsDistance = 10000000
		for x in xrange(0, len(distance)):
			angleError = math.fabs(trajectory[Heading][x] - kite[TrajectoryHeading])

			if (angleError > 180):
				angleError = 360 - angleError
 			if (angleError < headingError) and (distance[x] < minsDistance):
				indexDistance = x
				minsDistance = distance[x]
				
						
		return minsDistance, indexDistance
		
class KiteObject(object):
	def __init__(self):
		self.elevation = 0
		self.azimuth = 0
		self.heading = 0		#actual kite heading
		self.desiredHeading = 0 	#the heading with adjustment based on the distance
		self.trajectoryHeading = 0	#the heading without adjustment

	def getState(self, state):
		return getKiteStates[state]

	def setTrajectoryHeading(self, heading):
		self.trajectoryHeading = heading

	def getKiteStates(self):
		return [self.azimuth, self.elevation, self.heading, self.desiredHeading, self.trajectoryHeading]

	def setKiteStates(self, _kiteStates):
		self.azimuth = _kiteStates[Azimuth]
		self.elevation = _kiteStates[Elevation]
		self.heading = _kiteStates[Heading]
 		self.desiredHeading = _kiteStates[DesiredHeading]
		
	
class Trajectory(object):
	def __init__(self):
		self.elevation = []
		self.azimuth = []
		self.heading = []
		self.deltaA = []
		self.deltaE = []
	
		self.helper = HelperFunctions()
		self.createTrajectoryArrays()
	

	def createTrajectoryArrays(self):
		#elevation, azimuth
		for x in self.helper.dRange(0, 2 * math.pi, 0.01):
			self.azimuth.append(30 * math.sin(x))
			self.elevation.append(-10 * math.sin(2 * x) + 40)
		#heading
		for x in xrange(0, len(self.azimuth)):
			if (x - 1 == -1):
				arrayIndex = len(self.azimuth) - 1
			else:
				arrayIndex = x	
			self.deltaA.append(self.azimuth[arrayIndex] - self.azimuth[arrayIndex - 1])
			self.deltaE.append(self.elevation[arrayIndex] - self.elevation[arrayIndex - 1])
			self.heading.append(-math.atan2(self.deltaA[x], self.deltaE[x]) * 180 / math.pi)

	def getTrajectory(self):
		return [self.azimuth, self.elevation, self.heading]
	

class KiteController(object):
	def __init__(self):
		self.kite = KiteObject()
		self.previouskite = KiteObject()
		self.helper = HelperFunctions()
		self.trajectory = Trajectory()
		pass;
	
	def getAngleAdjustment(self, distance, signs, magnitude):
		#signs[0] = copysign(1,deltaE)
		#signs[1] = copysign(1,E)
		#signs[2] = copysign(1,A)
		#defines the angle adjustment based on distance based on the region space
		#ie whether the kite is within/outside the bounds of the desired trajectory
		return signs[0] * signs[1] * signs[2] * math.degrees(math.atan(distance / magnitude))

	def kiteControl(self, _kiteStates):
		self.previousKite = self.kite
		self.kite.setKiteStates(_kiteStates)
		previousKite = self.previousKite.getKiteStates()
		kite = self.kite.getKiteStates()
		print previousKite
		trajectory = self.trajectory.getTrajectory()
		dist, ind = self.helper.getClosestDistance(kite, trajectory, 360)

		if kite[Heading] > 90 and kite[Heading] < 180:
			dist, ind = self.helper.getClosestDistance(kite, trajectory, 45)
		if kite[Heading] > -180 and kite[Heading] < -90:	
			dist, ind = self.helper.getClosestDistance(kite, trajectory, 45)

		self.kite.setTrajectoryHeading(trajectory[Heading][ind])		

		signOutside = math.copysign(1, kite[Elevation] - trajectory[Elevation][ind])
		signAzimuth = math.copysign(1, trajectory[Azimuth][ind])
		signElevation = math.copysign(1, trajectory[Elevation][ind] - 39.5)
		p = [signOutside, signAzimuth, signElevation]
		zzzz = trajectory[Heading][ind]
		print "zzzzzzz"
		print zzzz
		if zzzz > 100:
			z = self.getAngleAdjustment(dist, p, 25)
		elif zzzz < -100:
			z = self.getAngleAdjustment(dist, p, 25)
		else:
			z = self.getAngleAdjustment(dist, p, 25)
		
		angleTarget = trajectory[Heading][ind] + z + 90
		#print angleTarget
		return angleTarget, ind
		
if __name__ == "__main__":
	Controller = KiteController()
	trajectory = Controller.trajectory.getTrajectory()
	#x.helper.plot1("Azimuth", trajectory[Azimuth])
	#dist, ind = x.helper.getClosestDistance(40,40,45, x.azimuth, elevation, heading, 90)	
	fieldX, fieldY = np.mgrid[-45:45:50j, 10:80:50j]
	fieldU = []
	fieldV = []
	plt.figure()
	plt.plot(trajectory[Azimuth], trajectory[Elevation])
	plt.axis([-40, 40, 10, 70])
	plt.ylabel('Elevation (Degrees)')
	plt.xlabel('Azimuth (Degrees)')
	#plt.savefig('testee.png', dpi=1200)
	plt.show()

	#Pointing in the direction of the trajectory angle at the closest point
	for x in range(0, len(fieldX)):
		arrayX = []
		arrayY = []
		for y in range(0, len(fieldX[x])):
			angleTarget, ind = Controller.kiteControl([fieldX[x][y], fieldY[x][y],0,0])
			angleTarget = math.radians(angleTarget)
			arrayX.append(math.cos(angleTarget))
			arrayY.append(math.sin(angleTarget))
		fieldU.append(arrayX)
		fieldV.append(arrayY)
	plt.quiver(fieldX, fieldY, fieldU, fieldV)
	plt.show()

	#pointing in the direction of the closest point
	for x in range(0, len(fieldX)):
		arrayX = []
		arrayY = []
		for y in range(0, len(fieldX[x])):
			Controller.kite.setTrajectoryHeading(0)
			angleTarget, ind = Controller.kiteControl([fieldX[x][y], fieldY[x][y],0,0])
			angleTarget = math.radians(angleTarget)
			

			
			arrayX.append(trajectory[Azimuth][ind] - fieldX[x][y])
			arrayY.append(trajectory[Elevation][ind] - fieldY[x][y])
		fieldU.append(arrayX)
		fieldV.append(arrayY)
	plt.figure()
	plt.ylabel('Elevation (Degrees)')
	plt.xlabel('Azimuth (Degrees)')
	plt.quiver(fieldX, fieldY, fieldU, fieldV)
	plt.show()




	for x in range(0,len(fieldX)):
		arrayX = []
		arrayY = []
		for y in range(0, len(fieldX[x])):
			dist, ind = Controller.helper.getClosestDistance(fieldX[x][y], fieldY[x][y], -135, trajectory[Azimuth], trajectory[Elevation], trajectory[Heading], 45)
			signOutside = math.copysign(1, fieldY[x][y] - trajectory[Elevation][ind])
			p = [signOutside, math.copysign(1,trajectory[Azimuth][ind]), math.copysign(1, trajectory[Elevation][ind] - 45)]
			z = Controller.getAngleAdjustment(dist, p)
			print z
			angleTarget = trajectory[Heading][ind] + 90 + z
			angleTarget = math.radians(angleTarget)
			print angleTarget
			arrayX.append(math.cos(angleTarget))
			arrayY.append(math.sin(angleTarget))
		fieldU.append(arrayX)
		fieldV.append(arrayY)
	print fieldV[0]
	plt.quiver(fieldX, fieldY, fieldU, fieldV)
	plt.show()
	#print x.azimuth[ind]
	#print x.elevation[ind]
	#x.helper.plot("Elevation vs Azimuth", x.azimuth, x.elevation, [-45,45,15,75])
	

