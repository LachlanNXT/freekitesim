# -*- coding: utf-8 -*-
from __future__ import division
"""
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
"""
""" Dynamic winch model. Assumptions:
    An asynchronous machine with a gearbox is used. The inertia
    of drum and motor are combined into one value (stiff coupling). """
from math import sqrt, pi
from Timer import Timer
#from Settings import pro

# CONSTANTS

WINCH_F_MAX = 4000
RADIUS   = 0.323 / 2.0                # drum radius            Edwin: r [m]
RATIO    = 6.2                        # gearbox ratio;         Edwin: n
INERTIA_M = 0.082                     # Inertia of the motor, as seen from the motor [kgm^2]
# TODO: Check for better total inertia value
# According to AE3T50 Case Study, Marc Boorsma, 6 June 2012: INERTIA_T = 0.204 [kgm^2]
INERTIA_T = 4 * INERTIA_M             # Inertia of the motor, gearbox and drum, as seen from the motor [kgm^2]
U_NOM    = 400.0 / sqrt(3)            # nominal motor voltage; Edwin: E_n [V]
OMEGA_SN = 1500.0 / 60. * 2. * pi     # Rated synchronous motor speed [rad/s]
OMEGA_MN = 1460.0 / 60. * 2. * pi     # Rated nominal motor speed [rad/s]
TAU_N    = 121.                       # Rated torque at nominal motor speed [Nm]
TAU_B    = 363.                       # Rated maximum torque for synchronous motor speed [Nm]
X = U_NOM**2 / (2 * OMEGA_SN * TAU_B) # Calculated motor reactance [Ohm]
L = X / OMEGA_SN                      # Calculated motor inductance [H]
# Calculated motor resistance [Ohm]
R2 = (U_NOM**2 * (OMEGA_SN - OMEGA_MN))/(2 * OMEGA_SN**2) * (1 / TAU_N + sqrt(1 / TAU_N**2 - 1 / TAU_B**2))
F_COULOMB = 122.0;                    # Coulomb friction [N]
TAU_STATIC = F_COULOMB * RADIUS / RATIO    # Coulomb friction torque [Nm]
C_VF = 30.6                           # Coefficient for the viscous friction        [Ns/m]
C_F = C_VF * RADIUS**2 / RATIO**2     # Coefficient for the viscous friction torque [Nms/rad]

BRAKE = True # if the break of the winch is active
V_MIN = 0.2  # minimal speed of the winch in m/s. If v_set is lower the brake is activated.
BRAKE_ACC = -25.0 # acceleration of the brake

# print R2, L

# smooth approximation of the sign function
EPSILON = 6
sign = lambda x: x / sqrt(x * x + EPSILON * EPSILON)

def calcAcceleration(set_speed, speed, force, useBrake = False):
    if useBrake:
        global BRAKE
        if abs(set_speed) < 0.9 * V_MIN:
            BRAKE = True
        if abs(set_speed) > 1.1 * V_MIN:
            BRAKE = False
        if BRAKE:
            return BRAKE_ACC * speed # if the brake is active the acceleration proportional to the speed
    omega      = RATIO/RADIUS * speed
    omega_sync = RATIO/RADIUS * set_speed
    delta = omega_sync - omega
    if abs(omega_sync) <= OMEGA_SN:
        omega_dot_m = (U_NOM**2 * R2 * delta) / (OMEGA_SN**2 * (R2**2 + L**2 * delta**2))
    else:
        omega_dot_m = (U_NOM**2 * R2 * delta) / (omega_sync**2 * (R2**2 + L**2 * delta**2))
    omega_dot_m += RADIUS / RATIO * force * 4000.0 / WINCH_F_MAX - C_F * omega - TAU_STATIC * sign(omega)
    omega_dot_m *= 1/INERTIA_T
    return RADIUS/RATIO * omega_dot_m

def getBrake():
    global BRAKE
    return BRAKE

def calcForce(set_speed, speed):
    """ Calculate the thether force as function of the synchronous tether speed and the speed. """
    acc = calcAcceleration(set_speed, speed, 0.0)
    return (RATIO/RADIUS) ** 2 * INERTIA_T * acc

if __name__ == "__main__":
    if True:
        print "Inertia, as seen from the generator: ", INERTIA_T
        print "C_F", C_F
        print "TAU_STATIC", TAU_STATIC
        print "R2, L", R2, L
        print "v_SN", (OMEGA_SN / (RATIO/RADIUS))
    if False:
        print 'Acceleration at 4 m/s due to friction: ', calcAcceleration(7.9, 8., 0.0)
    if False:
        from pylab import np, plot
        n = 256
        X = RATIO/RADIUS * np.linspace(-8.0, 8.0, n, endpoint=True)
        SIGN = []
        for i in range(n):
            SIGN.append(sign(X[i]))
        plot(X, SIGN, label='sign')
    if False:
        with Timer() as t0:
            for i in range(10000):
                pass
        with Timer() as t1:
            for i in range(10000):
                calcAcceleration(7.9, 8., 100.0)
        print "time for calcAcceleration  [µs]:   ", (t1.secs - t0.secs)  / 10000 * 1e6
    if False:
        from pylab import np, plot, xlim, ylim, legend, grid, gca
        force = calcForce(4.0*1.025, 4.0)
        print "Force: ", force
        n = 256
        F2, F4, F6, F7, F8 = [], [], [], [], []
        V = np.linspace(0.0, 9.0, n, endpoint=True)
        for i in range(n):
            F2.append(-calcForce(0.15, V[i]))
            F4.append(-calcForce(1.0, V[i]))
            F6.append(-calcForce(6.0, V[i]))
#            F7.append(-calcForce(-7.3, -V[i]))
            F8.append(-calcForce(8.0, V[i]))
        plot(V, F2, label=u'$v_s$ = 0.15 m/s')
        plot(V, F4, label=u'$v_s$ = 1 m/s')
        plot(V, F6, label=u'$v_s$ = 6 m/s')
#        plot(V, F7, label=u'$v_s$ = 7.3 m/s')
        plot(V, F8, label=u'$v_s$ = 8 m/s')
        ylim(-15000.0, 15000)
        #xlim(0.0, 9.0)
        xlim(0.0, 0.4)
        legend(loc='upper right')
        gca().set_ylabel(u'Tether force [N]')
        gca().set_xlabel(u'Reel-out speed [m/s]')
        grid(True, color='0.25')
    if False:
        from pylab import np, plot
        X = np.linspace(-307., 307., num = 1000)
        Y = []
        print "omega_max: ", RATIO/RADIUS * 8.
        for x in X:
            Y.append(sign(x))
        plot(X, Y)
    if True:
        print "f_max: ", WINCH_F_MAX






