# -*- coding: utf-8 -*-
"""
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
"""
"""
Model of a kite-power system in implicit form: residual = f(y, yd)

This model implements a 3D mass-spring system with reel-out. It uses six tether segments (the number can be
configured in the file Settings.py). The kite is modelled using four point masses with three attached plates.
The spring constant and the damping decrease with the segment length. The aerodynamic kite forces are
calculated, depending on reel-out speed, depower and steering settings.
Scientific background: http://arxiv.org/abs/1406.6218
"""

import numpy as np
# pylint: disable=E0611
from numba import double, int_, jit, __version__
# uncomment the following line to disable the numba compiler; usefull for debugging
# def jit(func): return func
from distutils.version import LooseVersion
assert (LooseVersion(__version__) >= LooseVersion("0.18")), "Numba 0.18.1 or higher required!"
import linalg_3d_v2 as la
from linalg_3d_v2 import add2, add3, sub2, sub3, div3, mul2, mul3, copy2, clear, norm, normalize1, \
                         normalize2, cross3, dot
import math
from math import exp, radians, pi, acos
from Settings import C_SPRING, DAMPING, SEGMENTS, L_0, V_REEL_OUT, MASS, KCU_MASS, ELEVATION, \
                     V_WIND, REL_SIDE_AREA, WINCH_MODEL, AREA, \
                     PARTICLES, MASSES, SPRINGS, KITE_PARTICLES, ALPHA_ZERO, ALPHA_ZTIP, WINCH_D_TETHER, AZIMUTH


from WinchModel import calcAcceleration
from KCU_Sim import calcAlphaDepower
from Timer import Timer
from Approximate_CLCD import approximate_cl, approximate_cd
from scipy.interpolate import InterpolatedUnivariateSpline

# TODO: add test_innerLoop

""" Calculate the aerodynamic kite properties. """
ALPHA_CL = [-180.0, -160.0, -90.0, -20.0, -10.0,  -5.0,  0.0, 20.0, 40.0, 90.0, 160.0, 180.0]
CL_LIST  = [   0.0,    0.5,   0.0,  0.08, 0.125,  0.15,  0.2,  1.0,  1.0,  0.0,  -0.5,   0.0]

ALPHA_CD = [-180.0, -170.0, -140.0, -90.0, -20.0, 0.0, 20.0, 90.0, 140.0, 170.0, 180.0]
CD_LIST  = [   0.5,    0.5,    0.5,   1.0,   0.2, 0.1,  0.2,  1.0,   0.5,   0.5,   0.5]

if False:
    calc_cl = InterpolatedUnivariateSpline(ALPHA_CL, CL_LIST)
    calc_cd = InterpolatedUnivariateSpline(ALPHA_CD, CD_LIST)
else:
    calc_cl = approximate_cl
    calc_cd = approximate_cd

# pylint: disable=E1101
G_EARTH      = np.array([0.0, 0.0, -9.81]) # gravitational acceleration
NO_PARTICLES = SEGMENTS + KITE_PARTICLES + 1

# TODO: Check, if KS should come from the kite definition file
KS = radians(16.565 * 1.064 * 0.875 * 1.033 * 0.9757 * 1.083)  # max steering

K_ds = 1.5 # influence of the depower angle on the steering sensitivity
MAX_ALPHA_DEPOWER = 31.0
K = 1 - REL_SIDE_AREA # correction factor for the drag
DRAG_CORR = 0.93       # correction of the drag for the 4-point model

C_D_TETHER = 0.958

EPSILON = 1e-6
RHO_0 = 1.225 # kg / m³

# C_0 = -0.0032 # steering offset
C_0 = 0.0 # steering offset

ALPHA = 1.0/7.0
REF_HEIGHT = 6.0
EXP = 0
LOG = 1
EXP_LOG = 2

@jit
def calcWindFactor(scalars, height):
    """ Calculate the wind speed at a given height and reference height. """
    profile_law = int_(scalars[Profile_law])
    if profile_law == EXP:
        return (height / REF_HEIGHT)**scalars[Alpha]
    elif profile_law == LOG:
        z_0 = 0.07
        return math.log(height / z_0) / math.log(REF_HEIGHT / z_0)
    else:
        # K = 4.8
        K = 1.0
        # z_0 = 0.000033
        z_0 = 0.0002
        log1 = math.log(height / z_0) / math.log(REF_HEIGHT / z_0);
        exp1 = (height / REF_HEIGHT)**scalars[Alpha]
        return log1 +  K * (log1 - exp1)

@jit
def calcWindHeight(scalars, v_wind_gnd, height):
    return v_wind_gnd * calcWindFactor(scalars, height)

@jit
def calcRho(height):
    return RHO_0 * exp(-height / 8550.0)

# constants for accessing the array of scalars
ParamCD       =  0
ParamCL       =  1
Length        =  2 # length of one tether segment at zero force
C_spring      =  3 # spring constant of one tether segment
Damping       =  4 # damping constant of one tether segment
Depower       =  5
Steering      =  6
# Alpha_depower =  7
Alpha_zero    =  8
Last_alpha    =  9 # unused
Psi           = 10 # heading angle in radian
Beta          = 11 # elevation angle in radian; initial value about 70 degrees
Cor_steering  = 12
D_tether      = 13
V_app_norm    = 14
Area          = 15
Last_v_app_norm_tether = 16
Stiffnes_factor = 17
Lift            = 18 # norm of the lift force (L2)
Drag            = 19 # norm of the sum of the drag forces (D2+D3+D4)
Alpha           = 20 # wind profile coefficient
Profile_law     = 21
SCALARS_SIZE    = 22

# constants for accessing the array of vec3
V_wind           =  0 # (westwind, downwind direction to the east)
V_wind_gnd       =  1
V_wind_tether    =  2 # wind at half of the height of the kite
Segment          =  3
Rel_vel          =  4
Av_vel           =  5
Unit_vector      =  7
Force            =  8
Last_force       =  9
Lift_force       = 10
Half_drag_force  = 11
Spring_force     = 12
Total_forces     = 13
Last_tether_drag = 14
V_apparent       = 15
V_app_perp       = 16
Kite_y           = 17
Steering_force   = 18
Acc              = 19
Temp             = 20
Vec_z            = 21
Last_alphas      = 22
V_app_2          = 23 # last apparent air velocity
Last_force2      = 24 # sum of the aerodynamic forces, acting on the kite
Pos2             = 25
Pos3             = 26
Pos4             = 27
V2               = 28
V3               = 29
V4               = 30
Va_2             = 31
Va_3             = 32
Va_4             = 33
Pos_centre       = 34
Delta            = 35
X                = 36
Y                = 37
Z                = 38
Va_xz2           = 39
Va_xy3           = 40
Va_xy4           = 41
DIR2             = 42
DIR3             = 43
DIR4             = 44
L2               = 45
L3               = 46
L4               = 47
D2               = 48
D3               = 49
D4               = 50

VEC3_SIZE        = 51

@jit
def calcParticleForces(vec3, pos1, pos2, vel1, vel2, v_wind_tether, spring, forces, stiffnes_factor, segments, \
                       d_tether, rho, i):
    """ Calculate the drag force of the tether segment, defined by the parameters po1, po2, vel1 and vel2
    and distribute it equally on the two particles, that are attached to the segment.
    The result is stored in the array self.forces. The array vec3 is used for intermediate
    results only. """
    p_1 = int_(spring[0])     # Index of point nr. 1
    p_2 = int_(spring[1])     # Index of point nr. 2
    l_0 = spring[2]           # Unstressed length
    k = spring[3] * stiffnes_factor       # Spring constant
    # print "---b->", k, spring[3], stiffnes_factor
    c = spring[4]             # Damping coefficient
    #print 'k, c: ', k, c
    sub3(pos1, pos2, vec3[Segment])   # segment = pos1 - pos2
    sub3(vel1, vel2, vec3[Rel_vel])   # rel_vel = vel1 - vel2
    la.add3(vel1, vel2, vec3[Av_vel]) # av_vel = 0.5 * (vel1 + vel2)
    la.mul2(0.5, vec3[Av_vel])        # dito
    norm1 = la.norm(vec3[Segment])
    if norm1 > EPSILON:
        div3(vec3[Segment], norm1, vec3[Unit_vector]) # unit_vector = segment / norm1
    else:
        clear(vec3[Unit_vector])
    k1 = 0.25 * k # compression stiffness kite segments
    k2 = 0.1 * k  # compression stiffness tether segments
    c1 = 6.0 * c  # damping kite segments
    # look at: http://en.wikipedia.org/wiki/Vector_projection
    # calculate the relative velocity in the direction of the spring (=segment)
    spring_vel   = la.dot(vec3[Unit_vector], vec3[Rel_vel])
    if (norm1 - l_0) > 0.0:
        if i >= segments:  # kite springs
             mul3(k *  (norm1 - l_0) + (c1 * spring_vel), vec3[Unit_vector], vec3[Spring_force])
        else:
             mul3(k *  (norm1 - l_0) + (c * spring_vel), vec3[Unit_vector], vec3[Spring_force])
    elif i >= segments: # kite spring
        mul3(k1 *  (norm1 - l_0) + (c1 * spring_vel), vec3[Unit_vector], vec3[Spring_force])
    else:
        mul3(k2 *  (norm1 - l_0) + (c * spring_vel), vec3[Unit_vector], vec3[Spring_force])
    # Aerodynamic damping for particles of the tether and kite
    sub3(v_wind_tether, vec3[Av_vel], vec3[V_apparent])
    area = norm1 * d_tether
    # v_app_perp = v_apparent - dot(v_apparent, unit_vector) * unit_vector
    mul3(dot(vec3[V_apparent], vec3[Unit_vector]), vec3[Unit_vector], vec3[Temp])
    sub3(vec3[V_apparent], vec3[Temp], vec3[V_app_perp])
    # half_drag_force = -0.25 * rho * C_D_TETHER * norm(v_app_perp, 2) * area * v_app_perp
    mul3(-0.25 * rho * C_D_TETHER * norm(vec3[V_app_perp]) * area, vec3[V_app_perp], vec3[Half_drag_force])
    add2(vec3[Half_drag_force], forces[p_1]) # forces[p_1] += half_drag_force + spring_force
    add2(vec3[Spring_force], forces[p_1])    # dito
    add2(vec3[Half_drag_force], forces[p_2]) # forces[p_2] += half_drag_force - spring_force
    sub2(vec3[Spring_force], forces[p_2])    # dito

@jit
def innerLoop2(vec3, scalars, pos, vel, v_wind_gnd, v_wind_tether, springs, forces, stiffnes_factor, segments, d_tether):
    for i in xrange(springs.shape[0]):
        # for i in xrange(1):
        p_1 = int_(springs[i, 0])  # First point nr.
        p_2 = int_(springs[i, 1])  # Second point nr.
        height = 0.5 * (pos[p_1][2] + pos[p_2][2])
        rho = RHO_0 * math.exp(-height / 8550.0)
        # print "height, wind_factor: ", height, calcWindFactor(scalars, height)
        v_wind_tether[0] = calcWindHeight(scalars, v_wind_gnd[0], height)
        # print "v_wind_tether[0], v_wind[0]: ", v_wind_tether[0], v_wind_gnd[0]
        v_wind_tether[1] = calcWindHeight(scalars, v_wind_gnd[1], height)
        v_wind_tether[2] = calcWindHeight(scalars, v_wind_gnd[2], height)
        calcParticleForces(vec3, pos[p_1], pos[p_2], vel[p_1], vel[p_2], v_wind_tether, springs[i], forces, \
                            stiffnes_factor, segments, d_tether, rho, i)

# pylint: disable=E1101
class FastKPS(object):
    """ Class with the inner properties of the residual calculations.  """
    def __init__(self):
        self.scalars = np.zeros(SCALARS_SIZE)
        self.scalars[ParamCD]  = 1.0
        self.scalars[ParamCL]  = 0.2
        self.scalars[Alpha] = 1.0/7.0
        self.scalars[Last_alpha] = double (0.1)
        self.scalars[Beta] = double(1.220) # elevation angle in radian; initial value about 70 degrees
        self.scalars[D_tether] = double(WINCH_D_TETHER)
        self.scalars[Stiffnes_factor] = 0.04

        self.vec3 = np.zeros((VEC3_SIZE, 3))
        self.vec3[V_wind, 0]        = V_WIND # (westwind, downwind direction to the east)
        self.vec3[V_wind_gnd, 0]    = V_WIND # (westwind, downwind direction to the east)
        self.vec3[V_wind_tether, 0] = V_WIND # wind at half of the height of the kite

        self.initial_masses = (np.ones(SEGMENTS + 1)) # array of the initial masses of the particles
        self.initial_masses *= double(MASS)
        self.masses = (np.ones(SEGMENTS + 1))
        copy2(np.array((0.1, ALPHA_ZTIP, ALPHA_ZTIP)), self.vec3[Last_alphas])

        self.pos = np.zeros((SEGMENTS + KITE_PARTICLES + 1, 3))
        self.vel = np.zeros((SEGMENTS + KITE_PARTICLES + 1, 3))
        # array of force vectors, one for each particle
        self.forces = np.zeros((SEGMENTS + PARTICLES.shape[0] - 1, 3))

@jit
def calcAeroForces4(scalars, vec3, forces, pos, vel, rho, alpha_depower, rel_steering):
    """
    scalars:          array of scalars
    vec3:             array of 3-component vectors
    pos2, pos3, pos4: position of the kite particles P2, P3, and P4
    v2, v3, v4:       velocity of the kite particles P2, P3, and P4
    rho:              air density [kg/m^3]
    rel_depower:      value between  0.0 and  1.0
    rel_steering:     value between -1.0 and +1.0
    """
    copy2(pos[SEGMENTS+2], vec3[Pos2])
    copy2(pos[SEGMENTS+3], vec3[Pos3])
    copy2(pos[SEGMENTS+4], vec3[Pos4])
    copy2(vel[SEGMENTS+2], vec3[V2])
    copy2(vel[SEGMENTS+3], vec3[V3])
    copy2(vel[SEGMENTS+4], vec3[V4])
    sub3(vec3[V_wind], vec3[V2], vec3[Va_2])              # va_2 = v_wind - v2
    sub3(vec3[V_wind], vec3[V3], vec3[Va_3])              # va_3 = v_wind - v3
    sub3(vec3[V_wind], vec3[V4], vec3[Va_4])              # va_4 = v_wind - v4
    add3(vec3[Pos3], vec3[Pos4], vec3[Pos_centre])        # pos_centre = 0.5 * (pos3 + pos4)
    mul2(0.5, vec3[Pos_centre])                           # dito
    sub3(vec3[Pos2], vec3[Pos_centre], vec3[Delta])       # delta = pos2 - pos_centre
    normalize2(vec3[Delta], vec3[Z])                      # z = -la.normalize(delta)
    mul2(-1.0, vec3[Z])                                   # dito
    sub3(vec3[Pos3], vec3[Pos4], vec3[Y])                 # y = la.normalize(pos3 - pos4)
    normalize1(vec3[Y])                                   # dito
    cross3(vec3[Y], vec3[Z], vec3[X])                     # x = la.cross(y, z)
    mul3(dot(vec3[Va_2], vec3[Y]), vec3[Y], vec3[Va_xz2]) # va_xz2 = va_2 - la.dot(va_2, y) * y
    sub3(vec3[Va_2], vec3[Va_xz2], vec3[Va_xz2])                        # dito
    if norm(vec3[Va_xz2]) < EPSILON:
        alpha_2 = np.nan
        clear(vec3[L2])
    else:
        normalize2(vec3[Va_xz2], vec3[Temp])              # dot2 = la.dot(la.normalize(va_xz2), x)
        dot2 = dot(vec3[Temp], vec3[X])                   # dito
        alpha_2 = (pi - acos(la.limit(dot2)) - alpha_depower) * 360.0 / pi + scalars[Alpha_zero]
        CL2, CD2 = approximate_cl(alpha_2), approximate_cd(alpha_2) * DRAG_CORR
        cross3(vec3[Va_2], vec3[Y], vec3[DIR2])           # DIR2 = la.cross(va_2, y)
        if norm(vec3[DIR2]) > EPSILON:
            # L2 = -0.5 * rho * (la.norm(va_xz2))**2 * AREA * CL2 * la.normalize(DIR2)
            normalize2(vec3[DIR2], vec3[L2])
            mul2(-0.5 * rho * (norm(vec3[Va_xz2]))**2 * AREA * CL2, vec3[L2])
        else:
            clear(vec3[L2])
    # D2 = -0.5 * K * rho * (la.norm(va_2))**2 * AREA * CD2 * va_2
    mul3(-0.5 * K * rho * (norm(vec3[Va_2])) * AREA * CD2, vec3[Va_2], vec3[D2])
    add2(vec3[L2], forces[SEGMENTS + 2])                  # self.forces[SEGMENTS + 2] += (L2 + D2)
    add2(vec3[D2], forces[SEGMENTS + 2])                  # dito

    mul3(dot(vec3[Va_3], vec3[Z]), vec3[Z], vec3[Va_xy3]) # va_xy3 = va_3 - la.dot(va_3, z) * z
    sub3(vec3[Va_3], vec3[Va_xy3], vec3[Va_xy3])          # dito
    if la.norm(vec3[Va_xy3]) < EPSILON:
        alpha_3 = np.nan
        clear(vec3[L3])
    else:
        normalize2(vec3[Va_xy3], vec3[Temp])              # dot3 = la.dot(la.normalize(va_xy3), x)
        dot3 = dot(vec3[Temp], vec3[X])                   # dito
        alpha_3 = (pi - acos(la.limit(dot3)) - rel_steering * KS) * 360.0 / pi + ALPHA_ZTIP
        CL3, CD3 = approximate_cl(alpha_3), approximate_cd(alpha_3) * DRAG_CORR
        cross3(vec3[Va_3], vec3[Z], vec3[DIR3])           # DIR3 = la.cross(va_3, z)
        if la.norm(vec3[DIR3]) > EPSILON:
            # L3 = -0.5 * rho * (la.norm(va_xy3))**2 * AREA * REL_SIDE_AREA * CL3 * la.normalize(DIR3)
            normalize2(vec3[DIR3], vec3[L3])
            mul2(-0.5 * rho * (norm(vec3[Va_xy3]))**2 * REL_SIDE_AREA * AREA * CL3, vec3[L3])
        else:
            clear(vec3[L3])

    # D3 = -0.5 * K * rho * la.norm(va_3) * AREA * REL_SIDE_AREA * CD3 * va_3
    mul3(-0.5 * K * rho * (norm(vec3[Va_3])) * AREA * REL_SIDE_AREA * CD3, vec3[Va_3], vec3[D3])
    add2(vec3[L3], forces[SEGMENTS + 3])                  # self.forces[SEGMENTS + 3] += (L3 + D3)
    add2(vec3[D3], forces[SEGMENTS + 3])                  # dito

    mul3(dot(vec3[Va_4], vec3[Z]), vec3[Z], vec3[Va_xy4]) # va_xy4 = va_4 - la.dot(va_4, z) * z
    sub3(vec3[Va_4], vec3[Va_xy4], vec3[Va_xy4])          # dito
    if la.norm(vec3[Va_xy4]) < EPSILON:
        alpha_4 = np.nan
    else:
        normalize2(vec3[Va_xy4], vec3[Temp])              # dot4 = double(la.dot(la.normalize(va_xy4), x))
        dot4 = dot(vec3[Temp], vec3[X])                   # dito
        alpha_4 = (pi - acos(la.limit(dot4)) + rel_steering * KS) * 360.0 / pi + ALPHA_ZTIP
        CL4, CD4 = approximate_cl(alpha_4), approximate_cd(alpha_4) * DRAG_CORR
        cross3(vec3[Z], vec3[Va_4], vec3[DIR4])           # DIR4 = la.cross(z, va_4)
        if la.norm(vec3[DIR4]) > EPSILON:
            # L4 = -0.5 * rho * (la.norm(va_xy4))**2 * AREA * REL_SIDE_AREA * CL4 * la.normalize(DIR4)
            normalize2(vec3[DIR4], vec3[L4])
            mul2(-0.5 * rho * (norm(vec3[Va_xy4]))**2 * AREA * REL_SIDE_AREA * CL4, vec3[L4])
        else:
            clear(vec3[L4])
    # D4 = -0.5 * K * rho * la.norm(va_4) * AREA * REL_SIDE_AREA * CD4 * va_4
    mul3(-0.5 * K * rho * (norm(vec3[Va_4])) * AREA * REL_SIDE_AREA * CD4, vec3[Va_4], vec3[D4])
    add2(vec3[L4], forces[SEGMENTS + 4])                  # self.forces[SEGMENTS + 4] += (L4 + D4)
    add2(vec3[D4], forces[SEGMENTS + 4])                  # dito

    vec3[Last_alphas][0] = alpha_2
    vec3[Last_alphas][1] = alpha_3
    vec3[Last_alphas][2] = alpha_4

    # calculate scalar values of lift and drag forces
    scalars[Lift] = norm(vec3[L2])
    copy2(vec3[D2], vec3[Temp])
    add2(vec3[D3], vec3[Temp])
    add2(vec3[D4], vec3[Temp])
    scalars[Drag] = norm(vec3[Temp])

@jit
def loop(scalars, vec3, masses, springs, forces, pos, vel, posd, veld, res0, res1):
    """
    Calculate the vector res0 and res1 using loops
    that iterate over all tether segments.
    Preconditions:
    scalars[D_tether] > 0
    scalars[Stiffnes_factor] > 0
    scalars[Damping] > 0
    scalars[Length] > 0
    scalars[C_spring] > 0
    scalars[Length] > 0
    np.all(masses > 0)
    """
    copy2(pos[0], res0[0]) # res0[0] = pos[0]
    copy2(vel[0], res1[0]) # res1[0] = vel[0]
    # res0[1:SEGMENTS+1] = vel[1:SEGMENTS+1] - posd[1:SEGMENTS+1]
    for i in xrange(1, NO_PARTICLES):
        sub3(vel[i], posd[i], res0[i])
    # Compute the masses and forces
    m_tether_particle = MASS * scalars[Length] / L_0
    masses[SEGMENTS] = KCU_MASS + 0.5 * m_tether_particle
    for i in xrange(0, SEGMENTS):
            masses[i] = m_tether_particle
            springs[i, 2] = scalars[Length] # Current unstressed length
            springs[i, 3] = scalars[C_spring] / scalars[Stiffnes_factor]
            springs[i, 4] = scalars[Damping]
    # print vec3[V_wind_gnd]
    # print vec3[V_wind_tether]
    # print scalars[D_tether]
    innerLoop2(vec3, scalars, pos, vel, vec3[V_wind_gnd], vec3[V_wind_tether], springs, forces, scalars[Stiffnes_factor], \
               int_(SEGMENTS), scalars[D_tether])
    for i in xrange(1, NO_PARTICLES):
        # res1[i] = veld[i] - (G_EARTH - self.forces[i] / MASSES[i])
        div3(forces[i], masses[i], vec3[Temp])
        sub3(G_EARTH, vec3[Temp], vec3[Temp])
        sub3(veld[i], vec3[Temp], res1[i])

class KPS(FastKPS):
    """ Class, that defines the physics of a kite-power system. """
    def __init__(self):
        FastKPS.__init__(self)
        self.res = np.zeros((SEGMENTS + PARTICLES.shape[0] - 1) * 6).reshape((2, -1, 3))
        if WINCH_MODEL:
            self.res = np.append(self.res, 0.0) # res_length
            self.res = np.append(self.res, 0.0) # res_v_reel_out
        self.t_0 = 0.0 # relative start time of the current time interval
        self.v_reel_out = 0.0
        self.sync_speed = 0.0
        self.l_tether = L_0 * SEGMENTS
        self.res0, self.res1 = self.res[0:-2].reshape((2, -1, 3))[0], self.res[0:-2].reshape((2, -1, 3))[1]
        self.rho = RHO_0
        self.alpha_depower = 0.0
        self.steering = 0.0

    def clear(self):
        """ Clear all member variables. """
        self.__init__()

    def residual(self, time, state_y, der_yd):
        """
        N-point tether model:
        Inputs:
        State vector state_y   = pos0, pos1, ..., posn-1, vel0, vel1, ..., veln-1, length, v_reel_out
        Derivative   der_yd    = vel0, vel1, ..., veln-1, acc0, acc1, ..., accn-1, lengthd, v_reel_out_d
        Output:
        Residual     res = res0, res1 = pos0,  ..., vel0, ...
        """
        # Reset the force vector to zero.
        self.forces.fill(0.0)

        length = state_y[-2]
        v_reel_out = state_y[-1]
        lengthd = der_yd[-2]
        v_reel_outd = der_yd[-1]
        part  = state_y[0:-2].reshape((2, -1, 3))  # reshape the state vector to a vector of particles
        # reshape the state derivative to a vector of particles derivatives
        partd = der_yd[0:-2].reshape((2, -1, 3))

        pos, vel = part[0], part[1]
        posd, veld = partd[0], partd[1]

        sync_speed = self.sync_speed
        self.scalars[Length] = length / SEGMENTS

        self.scalars[C_spring] = C_SPRING * L_0 / self.scalars[Length]
        self.scalars[Damping]  = DAMPING  * L_0 / self.scalars[Length]
        calcAeroForces4(self.scalars, self.vec3, self.forces, pos, vel, self.rho, self.alpha_depower, \
                        self.steering)
        loop(self.scalars, self.vec3, MASSES, SPRINGS, self.forces, pos, vel, posd, veld, self.res0, self.res1)
        self.v_reel_out = v_reel_out
        self.res[-2] = lengthd - v_reel_out
        self.last_acc = calcAcceleration(sync_speed, v_reel_out, la.norm(self.forces[0]), True)
        self.res[-1] = v_reel_outd - self.last_acc
        # self.res[-1] = v_reel_outd - calcAcceleration(sync_speed, v_reel_out, la.norm(self.forces[0]), True)
        return self.res

    def setSyncSpeed(self, sync_speed, t_0):
        """ Setter for the reel-out speed. Must be called every 50 ms (before each simulation).
        It also updates the tether length, therefore it must be called even if v_reelout has
        not changed. """
        # self.last_sync_speed = self.sync_speed
        self.sync_speed = sync_speed
        self.t_0 = t_0
        if t_0 <= 5.0:
            self.scalars[Alpha_zero] = t_0/5.0 * ALPHA_ZERO
        else:
            self.scalars[Alpha_zero] = ALPHA_ZERO
        # increase the stiffness of the bridle from 4% to 100% within the first 10 seconds
        if t_0 <= 10.0:
            self.scalars[Stiffnes_factor] = 9.6 * t_0 / 100.0 + 0.04
        else:
            self.scalars[Stiffnes_factor] = 1.0

    def setDepowerSteering(self, depower, steering):
        """ Setter depower and the steering model inputs. Valid range for steering: -1.0 .. 1.0.
        Valid range for depower: 0 .. 1.0 """
        self.alpha_depower = calcAlphaDepower(depower) * (MAX_ALPHA_DEPOWER / 31.0)
        self.steering = (steering - C_0) / (1.0 + K_ds * (self.alpha_depower / radians(MAX_ALPHA_DEPOWER)))

    def setL_Tether(self, l_tether):
        """ Setter for the tether reel-out lenght (at zero force). """
        self.l_tether = l_tether

    def getL_Tether(self):
        """ Getter for the tether reel-out lenght (at zero force). """
        return self.l_tether

    def get_acc(self):
        """ Getter for the tether acceleration. """
        return self.last_acc

    def getLoD(self):
        return self.scalars[Lift] / self.scalars[Drag]
        # TODO: implement this correctly
        # return 1.0
        # return self.lift_[0] / self.drag_[0]

    def getAlpha2(self):
        return self.vec3[Last_alphas][0]

    def getXYZ(self):
        #return self.vec_x, self.vec_y, self.vec_z
        return self.vec3[X], self.vec3[Y], self.vec3[Z]

    def getForce(self):
        """ Return the absolute value of the force at the winch as calculated during the last
        simulation. """
        return la.norm(self.forces[0]) # last_force is the force at the whinch!

    def getSpringForces(self, pos):
        """ Return an array of the scalar spring forces of all tether segements.
        Input: The vector pos of the positions of the point masses that belong to the tether. """
        forces = np.zeros(SEGMENTS)
        for i in range(SEGMENTS):
            forces[i] =  self.scalars[C_spring] * (la.norm(pos[i+1] - pos[i]) - self.scalars[Length])
        return forces

    def getV_Wind(self):
        """ Return the vector of the wind velocity at the height of the kite. """
        return self.vec3[V_wind]

    def getForceNegCnt(self):
        return 0

    def getNormZeroCnt(self):
        return 0

    # setV_WindGround(self, height, v_wind_gnd=V_WIND, wind_dir=0.0, v_wind = None, rel_turbulence = 0.0)
    def setV_WindGround(self, height, v_wind_gnd=V_WIND, wind_dir=0.0, v_wind = None, rel_turbulence = 0.0):
        """ Set the vector of the wind-velocity at the height of the kite. As parameter the height
        and the ground wind speed is needed.
        Must be called every 50 ms.
        """
        if height < 6.0:
            height = 6.0
        v_wind0 = v_wind_gnd * calcWindFactor(self.scalars, height) * math.cos(wind_dir)
        v_wind1 = v_wind_gnd * calcWindFactor(self.scalars, height) * math.sin(wind_dir)
        if v_wind is not None:
            self.vec3[V_wind][0]     = v_wind0 * (1-rel_turbulence) + v_wind[0] * rel_turbulence
            self.vec3[V_wind][1]     = v_wind1 * (1-rel_turbulence) + v_wind[1] * rel_turbulence
            self.vec3[V_wind][2]     =                                v_wind[2] * rel_turbulence
        else:
            self.vec3[V_wind][0] = v_wind0
            self.vec3[V_wind][1] = v_wind1
            self.vec3[V_wind][2] = 0.0
        # print 'v_wind[0]', self.v_wind[0], calcWindHeight(v_wind_gnd, height)
        self.vec3[V_wind_gnd][0] = v_wind_gnd * math.cos(wind_dir)
        self.vec3[V_wind_gnd][1] = v_wind_gnd * math.sin(wind_dir)
        # print "self.vec3[V_wind_gnd][0]", self.vec3[V_wind_gnd][0]
        # self.v_wind_tether[0] = calcWindHeight(v_wind_gnd, height / 2.0)
        self.rho = calcRho(height)

    def initTether(self):
        print "KPS4.initTether()..................", WINCH_D_TETHER, "m"
        self.scalars[D_tether] = WINCH_D_TETHER

    def setProfileLaw(self, profile_law):
        """
        profile_law is of type int
        Valid values:
        EXP = 0, LOG = 1, EXP_LOG=2
        """
        self.scalars[Profile_law] = profile_law

    def setAlpha(self, alpha):
        """
        Set the coefficient alpha of the exponential wind profile law.
        """
        self.scalars[Alpha] = alpha

    def init(self):
        """ Calculate the initial conditions y0, yd0 and sw0. Tether with the given elevation angle,
            particle zero fixed at origin. """
        self.initTether()

        DELTA = 1e-6
        # self.setCL_CD(10.0 / 180.0 * math.pi)
        pos, vel, acc = [], [], []
        state_y = DELTA
        vel_incr = 0
        sin_el, cos_el = math.sin(ELEVATION / 180.0 * math.pi), math.cos(ELEVATION / 180.0 * math.pi)
	print AZIMUTH
	print ELEVATION
	x_el = math.cos(ELEVATION / 180.0 * math.pi) * math.cos(AZIMUTH / 180.0 * math.pi)
	y_el = math.sin(ELEVATION / 180.0 * math.pi)
	z_el = math.sin(AZIMUTH / 180.0 * math.pi) * math.cos(ELEVATION / 180.0 * math.pi)

        for i in range(SEGMENTS + 1):
            radius = - i * L_0
            if i == 0:
                #pos.append(np.array([-cos_el * radius, DELTA, -sin_el * radius]))
		pos.append(np.array([-x_el * radius, DELTA + z_el * radius, -y_el * radius]))
                vel.append(np.array([DELTA, DELTA, DELTA]))
            else:
                #pos.append(np.array([-cos_el * radius, state_y, -sin_el * radius]))
		pos.append(np.array([-x_el * radius, DELTA + z_el * radius, -y_el * radius]))
                if i < SEGMENTS:
                    vel.append(np.array([DELTA, DELTA, -sin_el * vel_incr*i]))
                else:
                    vel.append(np.array([DELTA, DELTA, -sin_el * vel_incr*(i-1.0)]))
            acc.append(np.array([DELTA, DELTA, -9.81]))
	#HERE THE NEW KITE POSITION IS CALCULATED

	print "---Last tether position - USE THIS FOR THE NEW KITE POSITION---"
	print [x_el * (SEGMENTS) * L_0, DELTA + z_el * (SEGMENTS) * L_0, sin_el*(SEGMENTS) * L_0]

        state_y0, yd0 = np.array([]), np.array([])
        for i in range(SEGMENTS + 1):
            state_y0  = np.append(state_y0, pos[i]) # Initial state vector
            yd0 = np.append(yd0, vel[i]) # Initial state vector derivative
        # kite particles (pos and vel)

        for i in range(KITE_PARTICLES):
            state_y0  = np.append(state_y0, (PARTICLES[i+2])) # Initial state vector
            yd0 = np.append(yd0, (np.array([DELTA, DELTA, DELTA]))) # Initial state vector derivative
        for i in range(SEGMENTS + 1):
            state_y0  = np.append(state_y0, vel[i]) # Initial state vector
            yd0 = np.append(yd0, acc[i]) # Initial state vector derivative
        # kite particles (vel and acc)
        for i in range(KITE_PARTICLES):
            state_y0  = np.append(state_y0, (np.array([DELTA, DELTA, DELTA]))) # Initial state vector
            yd0 = np.append(yd0, (np.array([DELTA, DELTA, DELTA]))) # Initial state vector derivative
        self.setL_Tether(L_0 *  SEGMENTS)
        self.setSyncSpeed(V_REEL_OUT, 0.)
        # append the initial reel-out length and it's derivative
        state_y0 = np.append(state_y0, L_0 *  SEGMENTS)
        yd0 = np.append(yd0, V_REEL_OUT)
        # append reel-out velocity and it's derivative
        state_y0 = np.append(state_y0, V_REEL_OUT)
        yd0 = np.append(yd0, DELTA)
        return state_y0, yd0

def testCalcParticleForces():
    vec3 = np.zeros((22, 3))
    pos1, pos2, vel1, vel2 = np.zeros(3), np.ones(3), np.zeros(3), np.zeros(3)
    v_wind_tether, spring, forces, stiffnes_factor = np.ones(3), np.zeros(5), np.zeros((3,3)), 1.0
    segments, d_tether, i = 2, 3.3, 1
    rho = 1.25
    calcParticleForces(vec3, pos1, pos2, vel1, vel2, v_wind_tether, spring, forces, stiffnes_factor, segments, \
                   d_tether, rho, i)

def testInnerLoop2():
    vec3 = np.zeros((22, 3))
    pos, vel, v_wind = np.zeros((15,3)), np.zeros((15,3)), np.zeros(3)
    v_wind_tether, forces, stiffnes_factor = np.ones(3), np.zeros((15,3)), 1.0
    segments, d_tether = 2, 3.3
    kps = FastKPS()
    innerLoop2(vec3, kps.scalars, pos, vel, v_wind, v_wind_tether, SPRINGS, forces, stiffnes_factor, \
               segments, d_tether)

def testFastKPS():
    kps = FastKPS()
    return kps.scalars, kps.vec3, kps.forces

def testCalcAeroForces4():
    scalars, vec3, forces = testFastKPS()
    pos, vel = np.zeros((15,3)), np.zeros((15,3))
    rho = RHO_0
    alpha_depower, rel_steering = 0.0, 0.0
    calcAeroForces4(scalars, vec3, forces, pos, vel, rho, alpha_depower, rel_steering)

def testLoop():
    scalars, vec3, forces = testFastKPS()
    length = 150.0
    scalars[Length] = length / SEGMENTS
    pos, vel = np.zeros((15,3)), np.zeros((15,3))
    posd = vel.copy()
    veld = np.zeros_like(vel)
    res = np.zeros((SEGMENTS + PARTICLES.shape[0] - 1) * 6).reshape((2, -1, 3))
    if WINCH_MODEL:
        res = np.append(res, 0.0) # res_length
        res = np.append(res, 0.0) # res_v_reel_out
    res0, res1 = res[0:-2].reshape((2, -1, 3))[0], res[0:-2].reshape((2, -1, 3))[1]
    springs = SPRINGS.copy()
    loop(scalars, vec3, MASSES, springs, forces, pos, vel, posd, veld, res0, res1)

if __name__ == '__main__':
    np.set_printoptions(precision=4, suppress=True)
    testCalcParticleForces()
    testInnerLoop2()
    testFastKPS()
    testCalcAeroForces4()
    testLoop()
    # print SPRINGS
    PRINT = True
    MY_KPS = KPS()
    Y0, YD0 = MY_KPS.init() # basic consistency test; the result should be zero
    MY_KPS.setDepowerSteering(0.0, 0.0)
    if PRINT:
        print 'y0:\n' , Y0
        print 'yd0:\n', YD0
    LOOPS = 100
    RES = MY_KPS.residual(0.0, Y0, YD0)
    with Timer() as t:
        for j in range(LOOPS):
            RES = MY_KPS.residual(0.0, Y0, YD0)

    if PRINT:
        if WINCH_MODEL:
            print 'res:\n', (RES[0:-2].reshape((2, -1, 3)))
            print 'res_winch', RES[-2], RES[-1]
            MY_KPS.setV_WindGround(100.0)
        else:
            print 'res:', (RES.reshape((2, -1, 3)))
        print "\nExecution time (µs): ", t.secs / LOOPS * 1e6
    if False:
        print "Ground wind speed: ", V_WIND
        print "Wind at 200 m:     ", calcWindHeight(V_WIND, 200.0)
        print "Kite area:         ", AREA
        print "C_spring           ", MY_KPS.scalars[C_spring]
        print "Stiffnes_factor    ", MY_KPS.scalars[Stiffnes_factor]
        print "X, Y, Z: ", MY_KPS.getXYZ()
        print "MASSES", MASSES
        print "last_alphas ", MY_KPS.vec3[Last_alphas]
        print "forces", MY_KPS.forces
    # time, using without numba: 500 us on i7-3770,   3.4 GHz
    # time, using numba 0.18.2:   15 us on i7-3770,   3.4 GHz


