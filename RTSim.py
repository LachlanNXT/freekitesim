# -*- coding: utf-8 -*-
"""
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
"""
"""
Simulator for implicit problems given in the form:
  residual = f(y, yd, sw);
  y0, yd0, sw0 = init()
A the result is published using EstimatedSystemState and ParticleSystem messages.

It is assumed that the the first half of the y vector contains the positions of
3D particles. This particle vector is published using protobuf encoded messages
via the zeromq tcp port 5575.

The simulation is started when a START event is received from localhost:5563,
usually from the program ProtoLogger. The simulation is stopped with the STOP
event and terminated by the SYNC event.

The model is implemented in the seperate module KPS4P_v2.py. (KPS: Kite Power System).
To change the simulation parameters edit the file Settings.py.
"""
import sys
from os.path import expanduser
sys.path.append(expanduser("~") + '/00PythonSoftware/FreeKiteSim')
from KiteControl import KiteController
import numpy as np
import math
from math import atan2, pi, asin, radians
import time as ti
import matplotlib.pyplot as plt
from joy_pad import getSetValues, getStart, joystickPresent, getButtons
import asset.system_pb2 as system
#import threading
import traceback as tb
from Settings import SEGMENTS, MAX_ERROR, RADAU, KITE_PARTICLES, PERIOD_TIME, SLOW, \
                     PRINT_CPU_USAGE, WINCH_MODEL, V_REEL_OUT, REL_SPEED, DEBUG, \
                     V_WIND, AZIMUTH, WINCH_F_MAX, MODEL
VWind = V_WIND # default wind speed to be used if no GroundState message is received
WindDir = -AZIMUTH # default wind direction to be used if no GroundState message is received

from Logger import Logger
from KPS4P_v2 import KPS
# from KPS3_v2 import KPS
from Timer import Timer
from assimulo.problem import Implicit_Problem
if RADAU:
    from assimulo.solvers import Radau5DAE
else:
    from assimulo.solvers.sundials import IDA #Imports the solver IDA from Assimulo
from linalg_3d import calc_elevation, azimuth_north, norm
from cgkit.cgtypes import vec3

from Publisher import Publisher
from KCU_Sim import KCU_Sim
from trafo import calc_heading, calc_course


import gc
gc.disable()

if WINCH_MODEL:
    ONES    = np.ones((SEGMENTS + 1 + KITE_PARTICLES) * 6 + 2)
else:
    ONES    = np.ones((SEGMENTS + 1 + KITE_PARTICLES) * 6)
RUNNING = False                       # wait for START event from ProtoLogger
POD_CONTROL = False
ManOp = True

# Buttons on Logitech joystick
FIRE = 0  # Fire button
MIN = 2   # Button 3
MAX = 4   # Button 5
RESET = 1 # Button 2

# Depower settings
DEPOWER_MAX = 0.42
DEPOWER_MIN = 0.24
DEPOWER_AVG = 0.3

DEPOWER, STEERING = 0.2, 0.0
SYS_STATE = 0
NEW_CYCLE = False
NEW_WIND = False
PeriodTime = PERIOD_TIME
RelTurbulence = 0.0

MY_KPS = KPS()

def calcAzimuth(azimuth_north, upWindDirection = -pi/2.0):
    """ Calculate the azimuth in the wind reference frame.
        The upWindDirection is the direction the wind is coming from
        Zero is at north; clockwise positive. Default: Wind from west.
        Returns:
        Angle in radians. Zero straight downwind. Positive direction clockwise seen
        from above.
        Valid range: -pi .. pi. """
    result = azimuth_north - upWindDirection + pi
    if result > pi:
        result -= 2.0 * pi
    if result < -pi:
        result += 2.0 * pi
    return result
class ExtendedProblem(Implicit_Problem):
    """ Extend Assimulos problem definition with an initialization, a wrapper for the
        function that calculates the residual and a function that detects discontinuities. """

    # Set the initial conditons
    t0  = 0.0                   # Initial time
    y0, yd0 = MY_KPS.init()

    def res(self, t, y, yd):
        return MY_KPS.residual(t, y, yd)

class RT_Int(object):
    """ real-time integrator """
    def __init__(self):
        self.time = 0.0
        self.loop_count = 0
        self.logger = Logger()
	self.KC = KiteController()
        self.sync_speed = 0.0
        self.sync_base = 0.0
        self.max_time = 0.0 # maximal execution time of one simulation call
        self.simul_dur = 0.0 # duration of the last simulation in seconds
        self.time_v = []
        self.result_v = []
        self.pub = Publisher()
        self.kcu = KCU_Sim()
        #self.est = RTEstimator()
        self.this_sec = None
        self.supervised_sec = None
        self.delta_depower = 0.0
        self.set_depower = DEPOWER_AVG
        self.azimuth = 0.0
	self.elevation = 0.0
	self.heading = 0.0

	self.azimuthLog = []
	self.elevationLog = []
	self.headingLog = []

	self.trajHeading = 0.0
	self.trajHeadingPrev = 0.0

	self.errorPrev = 0.0
	self.previousAzimuth = 0.0
	self.previousElevation = 0.0
	self.previousHeading = 0.0
        self.last_force = 0.0
        self.av_force = 0.0
        #self.csec_old = 60 # ensures that the supervisor is called in the first loop
        mu = VWind
        sigma = 0.05
        self.wind_dist = np.random.normal(mu, sigma, 100000)
        # Create an instance of the problem.
        model = ExtendedProblem()   # Create the problem
        model.name = 'Tether-model' # Specifies the name of problem (optional)

        # Create the solver.
        if RADAU:
            self.imp_sim = Radau5DAE(model)
            self.imp_sim.inith = 0.003 # waS: 0.003
            self.imp_sim.thet = 0.09  # default: 0.002; was: 0.09
            self.fac1 = 0.3 # default: 0.2
            self.fac2 = 4.0 # default: 8.0
        else:
            self.imp_sim = IDA(model)
            self.imp_sim.inith = 0.002
            self.imp_sim.maxord = 3

        # Set the parameters of the solver.
        self.imp_sim.verbosity = 40 # valid range: 0 to 50; 50 means totally quiet
        pos_tol = MAX_ERROR/ 100.0
        vel_tol = pos_tol / 60.0
        self.imp_sim.atol = vel_tol * ONES
        for i in range(3 * (SEGMENTS + 1 + KITE_PARTICLES)):
            self.imp_sim.atol[i] = pos_tol
        if WINCH_MODEL:
            self.imp_sim.atol[-2] = pos_tol # tether length
            self.imp_sim.atol[-1] = vel_tol # reel-out velocity
        self.imp_sim.rtol = 1e-3 # was: 2.0e-5

    def updateWind(self):
        mu = VWind
        sigma = 0.05
        self.wind_dist = np.random.normal(mu, sigma, 50000)

    def step(self, prn = False):
        """ Calculate a new integration step.
            Returns the calculated state vector at the end of the time interval. """
        self.time += PeriodTime
        with Timer() as timer:
            if DEBUG:
                y1, yd1 = MY_KPS.init()
            else:
                try:
                    t1, y1, yd1 = self.imp_sim.simulate(self.time, 1) # Particle system solver call
                except Exception, ex:
                    print "##--------res-----------------##"
                    print MY_KPS.res
                    import traceback
                    print "##--------traceback-----------##"
                    print traceback.format_exc()
                    print "##--------end-----------------##"
                    raise ex

        self.simul_dur = timer.secs
        if self.simul_dur > self.max_time and self.time > 0.5:
            self.max_time = self.simul_dur
        if PRINT_CPU_USAGE:
            print '%.2f %%' % (self.simul_dur / (PeriodTime/ REL_SPEED) * 100.0)
        if prn:
            print round(self.time, 5), y1[1]
        if DEBUG:
            return y1
        else:
            return y1[1]
    def getDirection(self, targetAngle, actAngle):
	diff = targetAngle - actAngle
	if diff > 180:
		return -360 + diff
	elif diff < -180:
		return 360 + diff
	else:
		return diff
    def controlWinch(self, kiteStates):
        """ Read set value for steering and depower from joystick, if present. Set these values in
        the KCU object and publish them. """

	self.trajHeadingPrev = self.trajHeading
	#self.previousHeading = self.heading
	kiteStates[0] = math.degrees(kiteStates[0])
	kiteStates[1] = math.degrees(kiteStates[1])
	kiteStates[2] = math.degrees(kiteStates[2])
	if kiteStates[2] > 180:
		kiteStates[2] = - (360 - kiteStates[2])
	angle, ind = self.KC.kiteControl(kiteStates)
	angle = angle - 90

	self.trajHeading = angle
	a = self.getDirection(angle, kiteStates[2])
	print "Ta"
	print angle
	print "Ka"
	print kiteStates[2]
	print "a"
	print a
	error = a

	errorDot = error - self.errorPrev
	
	self.errorPrev = error
	
	kiteVel = kiteStates[2] - self.previousHeading
	
	#print error
	print 'loop'
	print kiteStates[2]
	print angle
	set_steering = -(error + 100 * errorDot)

	if (kiteVel > 5) and set_steering < 0:
		set_steering = 0
	if kiteVel < -5 and set_steering > 0:
		set_steering = 0
	#print angle

	self.kcu.setDepowerSteering(self.set_depower, set_steering)
	depower, steering = self.kcu.getDepower(), self.kcu.getSteering()
	self.kcu.onTimer()
	MY_KPS.setDepowerSteering(depower, steering)
	self.previousHeading = kiteStates[2]


        self.sync_speed = 0


    def simulate(self, plot = True, prn = False):
        global RUNNING, NEW_CYCLE, NEW_WIND, ManOp, RelTurbulence
        #interval = PeriodTime
        self.time = 0.0
        old_time = ti.time()
        self.sync_speed = 0.0
        self.imp_sim.reset()
        self.logger.clear()
        print "WINCH_MODEL: ", WINCH_MODEL
        print
        print
        try:
            while RUNNING:
                if NEW_WIND:
                    self.updateWind()
                    NEW_WIND = False
                # wait until the next simulation interval starts
                while (math.fabs(math.fmod(ti.time(),  PeriodTime/ REL_SPEED)) > 5e-4):
                    ti.sleep(0.0002)
		self.previousAzimuth = self.azimuth
		self.previousElevation = self.elevation

                self.controlWinch([self.azimuth, self.elevation, self.heading, self.KC.kite.desiredHeading, self.KC.kite.trajectoryHeading])
                if WINCH_MODEL:
                    MY_KPS.setSyncSpeed(self.sync_speed, self.time)
                    #print "MY_KPS.setSyncSpeed: ", self.sync_speed
                else:
                    MY_KPS.setV_ReelOut(self.sync_speed, self.time, PeriodTime)
                delta_t = ti.time() - old_time
                old_time += delta_t
                # simulate
                y1 = self.step(prn=prn)
                # process the results
                if V_REEL_OUT is None:
                    v_reel_out = 0.0
                else:
                    if WINCH_MODEL:
                        v_reel_out = y1[-1]
                    else:
                        v_reel_out = self.sync_speed
                if WINCH_MODEL:
                    part = y1[0:-2].reshape((2, -1, 3))
                else:
                    part = y1.reshape((2, -1, 3))    # reshape the state vector to a vector of particles
                pos, vel = part[0], part[1]

                pos_kite = 0.5 * (pos[SEGMENTS+3] + pos[SEGMENTS+4])
                height = pos_kite[2]
                kite_distance = np.linalg.norm(pos_kite)

                vel_kite = 0.5 * (vel[SEGMENTS+3] + vel[SEGMENTS+4])

                v_wind_ground = self.wind_dist[self.loop_count]

                MY_KPS.setV_WindGround(pos_kite[2], v_wind_ground, WindDir / 180.0 * pi)

                azimuth_north1 = azimuth_north(pos_kite)
                azimuth, elevation = calcAzimuth(azimuth_north1), calc_elevation(pos_kite)
                self.azimuth = azimuth
		self.elevation = elevation

		self.azimuthLog.append(math.degrees(self.azimuth))
		self.elevationLog.append(math.degrees(self.elevation))
                v_app = vec3(MY_KPS.getV_Wind() - vel_kite)
                v_app_norm = v_app.length()
                self.delta_depower = 0.0

                # calculate the X, Y and Z vectors of the kite reference frame
                x, y, z = MY_KPS.getXYZ()
                alpha = radians(MY_KPS.getAlpha2())

                # calculate the Euler angles
                roll = atan2(y[2], z[2]) - pi/2.0
                if roll < -pi/2.0:
                    roll += 2 * pi
                pitch = asin(-x[2])
                yaw = -atan2(x[1], x[0]) - pi/2.0
                if yaw < -pi/2.0:
                    yaw += 2 * pi
                orient = vec3(roll, pitch, yaw)

                length = 0.0
                for i in xrange(SEGMENTS, 0, -1):
                    length += norm(pos[i] - pos[i-1])
                heading = calc_heading(orient, elevation, azimuth)
		self.heading = heading
                course = calc_course(vel_kite, elevation, azimuth)
                force = MY_KPS.getForce()
                if math.isnan(force):
                    print pos, vel
                assert not math.isnan(force)
                self.av_force = 0.5 * (force + self.last_force)

                LoD = MY_KPS.getLoD()
                v_wind_kite = vec3(MY_KPS.getV_Wind()).length()


                prediction = []
                cpu_time = self.simul_dur/(PeriodTime/ REL_SPEED) * 100.0

                # publish the array of position vectors, v_reel_out, v_app, l_tether, force
                #    azimuth and elevation in a ParticleSystem message
                #v_wind_height = vec3(MY_KPS.getV_Wind()).length()
                if not SLOW or self.loop_count % 2 == 0:
                    #self.pub.publishParticleSystem(pos, v_reel_out, v_wind_height, v_app, length, self.av_force, \
                    #             azimuth, elevation, vel_kite, prediction, period_time = PeriodTime)
                    self.pub.publishParticleSystem(pos, v_reel_out, v_app, length, self.av_force, \
                              azimuth, elevation, vel_kite, cpu_time, \
                              self.kcu.getDepower(), prediction)

                # Append the calculated data to the logfile arrays
                self.logger.append(self.time, self.av_force, height, length, kite_distance, v_reel_out, \
                                   self.sync_speed)
                self.logger.append2(azimuth, elevation, v_app_norm, self.kcu.getDepower(), \
                                   self.kcu.getSteering(), yaw, heading, course, \
                                   v_wind_ground)
                # print "v_t,b v_reel_out", self.sync_base, v_reel_out

                self.logger.append3(pos, vel_kite, v_app, prediction, alpha, vel, cpu_time=cpu_time, LoD=LoD, \
                                    v_tb=self.sync_base)
                self.logger.append4(DEPOWER, STEERING, v_wind_kite)


                self.last_force = force
                #TODO CF remove weak link?
                if force > 1.5 * WINCH_F_MAX:
                    print "Weak link breaks!"
                    RUNNING = False
                if pos[-3][2] < 5:
                    print "Position too low!"
                    RUNNING = False
                if plot:
                    self.time_v.append(self.time)
                    self.result_v.append(y1)
                self.loop_count += 1
        except KeyboardInterrupt:
            print "Simulating ended through Keyboard Interrupt"
            RUNNING=False
	    #MAKE THE PLOTS : >
	    

        except Exception, ex:
            print str(ex)
            tb.print_exc()
            RUNNING = False
        finally:
	    Ta, Te, Th = self.KC.trajectory.getTrajectory()
	    plt.figure()
	    plt.plot(self.azimuthLog, self.elevationLog)
	    plt.plot(Ta, Te)
	    plt.show()
	    print "plots"
            self.save_log()

    def save_log(self):
        if self.loop_count > 0:
            df = self.logger.getDataFrame()
            df.time_saved = ti.time()
            df.model = MODEL
            print
            self.logger.save(convert=False)


#TODO CF for running the simulation we have RTSim and the 3D viewer (Viewer may need to be updated)
if __name__ == '__main__':
    print "Ready to go!"
    print "Simulation started!"
    RUNNING = True

    while 1:
        if RUNNING:

            rt_int = RT_Int()
            MY_KPS.initTether()
            ti.sleep(0.2)
            rt_int.simulate(plot=False, prn=False)
            rt_int.kcu.clear()
            ti.sleep(0.2)
            rt_int.pub.close()
            ti.sleep(0.2)
            rt_int = None
            MY_KPS = None
            ti.sleep(0.2)
            MY_KPS = KPS()
            ti.sleep(0.2)
        else:
            break
