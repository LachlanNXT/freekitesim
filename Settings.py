"""
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
"""
"""
Simulation constants.
Imported from RTSimulator.py, KiteViewer3.py, KPS4P.py.
"""
import numpy.linalg as la
import numpy as np
import math
from initialConditions import AZIMUTH, ELEVATION
from os.path import expanduser
from os import environ
PATH = expanduser("~") + '/00PythonSoftware/FreeKiteSim'
SLOW = False # slow update rate of 3D viewer (10 Hz instead of 20 Hz)
USE_WIND_FIELD = False

# The example is controlled by an envionment variable which needs to be set.
# example 1: replaying log files, using the 1-point kite model
# example 2: real-time simulation, using the 4-point kite model
if 'EXAMPLE' in environ.keys():
    EXAMPLE = int(environ['EXAMPLE'])
else:
# If no envionment variable is set, it will be assumed that the simulation is
# supposed to be played
    EXAMPLE = 2

WINCH_F_MAX = 4000
WINCH_D_TETHER = 4e-3

from Kite_4_point import Kite_4_point

#TODO: Check if 55e9 or 55e7 is correct
E_DYNEEMA =  55e9        # Dyneema E-modulus [Pa] (according to M.B. Ruppert)
M_DYNEEMA = 724.0        # Density of Dyneema kg/m^3
SCALE_TETHER = 1.0
PLATE = False            # default, can be overridden later
NO_REELOUT = False
ALPHA_ZERO = 0.0
ALPHA_ZTIP  = 0.0 # should be 10
VIDEO = False
HEIGHT_K = 2.0         # default, needed by KCU_Sim
POWER2STEER_DIST = 1.3 # default, needed by KCU_Sim
STEERING_COEFFICIENT = 2.59184 # default, needed by KPS3.py
BRIDLE_DRAG = 1.1

if EXAMPLE == 1:         # point-mass kite model
    REL_SPEED = 1.0      # simulation speed relative to reel-time
    PRINT_CPU_USAGE = False
    ELEVATION = 60       # initial elevation angle in degrees
    KITE_MASS = 11.4     # kite including sensor unit
    KCU_MASS  =  8.4
    SEGMENTS =  6        # number of tether segments
    # List of springs. [x_n  x_k  L_0  elements]
    SPRINGS_INPUT = np.loadtxt(PATH+"/input/default.txt", comments = '%')
    if SPRINGS_INPUT.ndim > 1:
        L_TOT_0 = SPRINGS_INPUT[0, 2]       # initial tether length [m]
    else:
        L_TOT_0 = SPRINGS_INPUT[2]       # initial tether length [m]
    L_0     = L_TOT_0 / SEGMENTS  # initial segment length [m]
    HEIGHT_K = 0.0
    HEIGHT_B = L_0
    D_TETHER = 4 * 4e-3  # tether diameter
    C_SPRING = 6.146e5 / L_0  # initial spring constant  [N/m]
    DAMPING  = 2 * 473.0 / L_0    # initial damping constant [Ns/m]
    MASS = 0.011 * L_0   # initial mass per particle: 1.1 kg per 100m = 0.011 kg/m for 4mm Dyneema
    V_REEL_OUT = 4.0     # ree-out speed in m/s (None: fixed tether length)
    MAX_ERROR = 2        # maximal position error in cm
    RADAU = True
    NUMBA = True
    MODEL = 'KPS3'
    V_WIND = 8.0         # wind speed in 6 m height
    REL_SIDE_AREA = 0.5
    STEERING_COEFFICIENT = 0.6
    KITE_PARTICLES = 0   # Number of particles for the kite, that do not belong to the tether
    F_MAX = 4000         # max winch force
    F_MIN =  300         # min winch force
    F_RO =  3800         # desired reel-out force
    F_RI =  1700         # desired reel-in force
    PERIOD_TIME = 0.05
    WINCH_MODEL = True
    BUSY_WAITING = False
    SCALE = 40           # meters per tick mark
    KITE_SCALE = 8.0     # enlargement factor of the kite to improve visibility


if EXAMPLE == 2:         # EX2: normal 4-point kite model EX4: 4-point kite model using 5th line
    USE_PREDICTION = True
    C2_COR =  0.93
    MAASVLAKTE = False
    SEGMENTS =  6        # number of tether segments was: 6
    M_k = 0.2  # relative nose distance, default: 0.2; increasing M_k increases C2 of the turn-rate law
    REL_NOSE_MASS = 0.47 # relative nose mass, default: 0.47
    REL_TOP_MASS  = 0.4  # mass of the top particle relative to the sum of top and side particles, def: 0.4
    AREA = 10.18 
    HEIGHT_K = 2.23 
    HEIGHT_B = 4.9 
    POWER2STEER_DIST = 1.3 
    KITE_MASS = 6.21 
    WIDTH = 5.77 * 0.86 
    REL_SIDE_AREA = 0.306 
    DEPOWER_OFFSET = 23.6

    MODEL = 'KPS4'
    if MODEL == 'KPS4':
        kite = Kite_4_point(HEIGHT_K, HEIGHT_B, WIDTH, SEGMENTS, M_k)
        PARTICLES = kite.getParticles()
    	KITE_PARTICLES = 4   # one kite particle belongs to the tether
        # List of springs. [x_n  x_k  L_0  elements]
    	SPRINGS_INPUT = np.loadtxt(PATH+"/input/springs_four_point_kite.txt", comments = '%')
    # print 'AREA, HEIGHT_K, HEIGHT_B, KITE_MASS, WIDTH: ', AREA, HEIGHT_K, HEIGHT_B, KITE_MASS, WIDTH
    # print 'REL_SIDE_AREA', REL_SIDE_AREA
    # print PARTICLES
    VIDEO = False
    # E_DYNEEMA =  4*55e7    # only used for the bridle; correct value: 55e9
    ALPHA_ZERO =   4.0  # should be 5
    ALPHA_ZTIP  = 10.0 # should be 10
    DEBUG = False
    NO_REELOUT = False
    REL_SPEED = 1.0    # simulation speed relative to real-time
    PRINT_CPU_USAGE = False
    #ELEVATION = 35       # initial elevation angle in degrees
    #AZIMUTH = 0
    KCU_MASS  =  8.4


    if SPRINGS_INPUT.ndim > 1:
        L_TOT_0 = SPRINGS_INPUT[0, 2]       # initial tether length [m]
    else:
        L_TOT_0 = SPRINGS_INPUT[2]       # initial tether length [m]
    STABILISATION = False    # Run stabilisation springs
    L_0      = L_TOT_0 / SEGMENTS  # initial segment length [m]
    print L_0
    print "^L_0"
    D_TETHER = 4e-3  # tether diameter
    D_BRIDLE = 2.5e-3     # diameter of the bridle lines [m]
    C_SPRING = 6.146e5 / L_0  # initial spring constant  [N/m]
    DAMPING  = 2 * 473.0 / L_0    # initial damping constant [Ns/m]
    MASS = 0.011 * L_0   # initial mass per particle: 1.1 kg per 100m = 0.011 kg/m for 4mm Dyneema
    V_REEL_OUT = 0.0     # ree-out speed in m/s
    MAX_ERROR = 1.8        # maximal position error in cm
    RADAU = True

    V_WIND  = 9.39         # wind speed in 6 m height
    #AZIMUTH = 0.0          # default azimuth; can be changed in the test scripts
    # STEERING_COEFFICIENT = 0.6

    F_MAX = 4000         # max winch force
    F_MIN =  300         # min winch force
    if USE_PREDICTION:
        F_RO =  3850         # desired reel-out force
    else:
        if MAASVLAKTE:
            F_RO = 3100
        else:
            F_RO =  3650         # desired reel-out force
    F_RI =   740         # desired reel-in force
    PERIOD_TIME = 0.05
    WINCH_MODEL = True
    BUSY_WAITING = False
    SCALE = 40           # meters per tick mark
    SCALE_TETHER = 1.0
    KITE_SCALE = 5.0
    PRE_STRESS = 0.9998   # Multiplier for the initial spring lengths.


# The springs are created here
# SPRINGS_INPUT is an array of segments. For each segment the first point, the last point and
# the unstressed length are defined. There are two exeptions:
# The first entry always represents the main tether, which is split into SEGMENTS segments later.
# For all other entries - if any - the unstressed length should be -1
if SPRINGS_INPUT.ndim < 2:
    # if only a main tether is used.
    # SPRINGS is an array of segments.
    # For each segment a vector of the index of the start and endpoint, the unstressed length and
    # the spring constant k and the damping coefficient c is defined
    SPRINGS = np.zeros((SEGMENTS, 5))
    NR_INPUT_SPRINGS = 1
else:
    SPRINGS = np.zeros((SEGMENTS + SPRINGS_INPUT.shape[0] - 1, 5))
    NR_INPUT_SPRINGS = SPRINGS_INPUT.shape[0]
if EXAMPLE == 4 or MODEL == 'KPS4':
    MASSES = np.zeros((SEGMENTS + PARTICLES.shape[0] - 1))
    # iterate over the array SPRING_INPUT and build the arrays SPRINGS and MASSES
    if not PLATE:
        MASSES[SEGMENTS] += KCU_MASS            # At the end of the tether add the KCU mass.
    if MODEL == 'KPS4':
        k2 = REL_TOP_MASS * (1.0 - REL_NOSE_MASS)
        k3 = 0.5 * (1.0 - REL_TOP_MASS) * (1.0 - REL_NOSE_MASS)
        k4 = 0.5 * (1.0 - REL_TOP_MASS) * (1.0 - REL_NOSE_MASS)
        MASSES[SEGMENTS+1] += REL_NOSE_MASS * KITE_MASS
        MASSES[SEGMENTS+2] += k2 * KITE_MASS
        MASSES[SEGMENTS+3] += k3 * KITE_MASS
        MASSES[SEGMENTS+4] += k4 * KITE_MASS

if MODEL == 'KPS4' or MODEL == 'KPS3':
    for j in xrange(SPRINGS_INPUT.shape[0]):
        if (j == 0 or SPRINGS_INPUT.ndim == 1) and not PLATE:      # if spring == tether
            # build the tether segments
            for i in range(SEGMENTS):
                if i <= SEGMENTS:
                    k = E_DYNEEMA * (D_TETHER/2.0)**2 * math.pi  / L_0  # Spring stiffness for this spring [N/m]
                    c = DAMPING                     # Damping coefficient [Ns/m]
                    SPRINGS[i,:] = np.array([i, i+1, L_0, k, c])   # Fill the SPRINGS
                    m_ind0 = SPRINGS[i, 0]          # index in pos for mass
                    m_ind1 = SPRINGS[i, 1]          # index in pos for mass
                    # Fill the mass vector
                    if MODEL != 'KPS3':
                        MASSES[m_ind0] += 0.5 * L_0 * M_DYNEEMA * (D_TETHER/2.0)**2 * math.pi
                        MASSES[m_ind1] += 0.5 * L_0 * M_DYNEEMA * (D_TETHER/2.0)**2 * math.pi
        else:
            # build the bridle segments
            p0, p1 = SPRINGS_INPUT[j, 0], SPRINGS_INPUT[j, 1] # Point 0 and 1
            # If the unstressed length is not specified use the distance between
            # the two particles.
            if SPRINGS_INPUT[j, 2] == -1:
                # Give the line a small amount of pre-stress
                l_0 = la.norm(PARTICLES[p1] - PARTICLES[p0]) * PRE_STRESS
            if SPRINGS_INPUT[j, 2] != -1:
                # Give the line the pre-stress as defined in the input file.
                PRE_STRESS = SPRINGS_INPUT[j, 2]
                l_0 = la.norm(PARTICLES[p1] - PARTICLES[p0]) * PRE_STRESS
            if p1 > 31  or p0 > 31:
                k = E_DYNEEMA * (D_BRIDLE/2.0)**2 * math.pi / l_0
            else:
                # The front of the bridle has 4mm^2 cable
                k = E_DYNEEMA * (D_TETHER/2.0)**2 * math.pi / l_0
            p0 += SEGMENTS - 1 # correct the index for the start and end particles of the bridle
            p1 += SEGMENTS - 1
            assert (l_0 != 0)
            c = DAMPING * L_0 / l_0
            SPRINGS[j + SEGMENTS - 1, :] = np.array([p0, p1, l_0, k, c])

# Nr. of springs
NO_SPRINGS = SPRINGS.shape[0]

if MODEL == 'KPS4' or MODEL == 'KPS3':
    SPRINGS_F = np.asfortranarray(SPRINGS)

print "MODEL: ", MODEL

#try:
#    import numba
#    print "Imported numba: ", numba.__version__
#except:
#    print "Warning: Failed to import numba!"
#    NUMBA = False

if __name__ == "__main__":
    print 'SPRINGS: \n', SPRINGS
    print 'SEGMENTS: ', SEGMENTS
    print 'SPRINGS.shape[0]: ',SPRINGS.shape[0]
    print 'KITE_PARTICLES', KITE_PARTICLES
    print 'PARTICLES', PARTICLES
    print 'C_SPRING', C_SPRING
    if MODEL == 'KPS4':
        print 'MASSES = ', MASSES
        print 'DEPOWER_OFFSET', DEPOWER_OFFSET

