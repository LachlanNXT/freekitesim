import sys
import os
import urllib

USER = os.path.expanduser("~")+'/'
KITE_ENV = USER+"00PythonSoftware/"
PYTHON = "http://repo.continuum.io/pkgs/free/linux-64/python-2.7.8-1.tar.bz2"
REQUESTS = "http://repo.continuum.io/pkgs/free/linux-64/requests-2.4.0-py27_0.tar.bz2"

def install_base(self):
    """""
    Installs the base environment if it does not exist yet
    """
    print "Installing base system"

    # path = raw_input("Path to Environment(if it does not exist yet install path)? (default[ENTER] $HOME/00PythonSoftware) ")
    path = ''
    if path == '':
        path = KITE_ENV
    
    path = expanduser(expandvars(path))
    # Make sure that the last character is a slash such that file names can
    # easily be appended to the path name
    if not path[-1] == '/':
        path = path + '/'

    try:
        os.makedirs(path)
        # Create neccessary environment folders
        os.mkdir(path+"/bin")
        os.mkdir(path+"/lib")
        os.mkdir(path+"/include")
        os.mkdir(path+"pkgs")
    except OSError as err:
        if err.errno!=17:
            raise

    # TODO: Move dependency management to .package_index
    # open(path+"/.package_index", 'a').close()
    config = {}
    config['home'] = path
    config['installed'] = []
    config['channels'] = ["https://conda.binstar.org/ufechner/linux-64/", 
                          "https://conda.binstar.org/janharms/linux-64/", 
                          "https://conda.binstar.org/mutirri/linux-64/", 
                          "http://repo.continuum.io/pkgs/free/linux-64/"]
    config['software'] = KITE_ENV
    config['strict'] = []
    config['larger'] = []
    config['depends'] = {}

    # Check status of installed Software
    ls = os.listdir(self.config['software'])
    if "FreeKiteSim" in ls:
        fks_exists = True

    if not fks_exists:
        try:
            gitClone('https://bitbucket.org/ufechner/FreeKiteSim.git', KITE_ENV)
        except OSError:
            print("""git seems not to be installed. Please install it. For Ubunutu run:\n\tsudo apt-get install git""")
            sys.exit()

    
